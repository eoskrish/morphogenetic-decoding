function [val,isterm,dir] = eventfun_nT2B(t,y)

% This is a function to determine the stop condition for the integrator
% employed, e.g. ODE15s

global nT
global rp
global fp
global Lx
global psi
global phi
global top
global ny

% tolerance for dydt (approximation to zero for steady state)
tol = 1e-4;

z = reshape(y,[ny,2*nT+1]);
r = repmat(rp,[ny,1]);

rate_id = top(2);
if rate_id <= 2 
    rate_id = rate_id;
elseif rate_id <= 3*nT+2
    rate_id = rate_id - 1;
else
    rate_id = rate_id - 2;
end

r0 = rp(rate_id);
if top(3) == 0
    r(:,rate_id) = r0./(1 + (fp(1)*z(:,top(1))).^fp(2));
else
    r(:,rate_id) = r0*(1 + (fp(1)*z(:,top(1)).^fp(3))./(fp(2)^(-fp(3)) + z(:,top(1)).^fp(3)) );
end

rD = 1;
kD = 1;
if nT == 1
    dydt(:,1) = r(:,1).*Lx'.*(psi-z(:,1)) - (rD + r(:,2)).*z(:,1) - r(:,6*nT-1).*z(:,nT).*z(:,2*nT) + r(:,6*nT).*z(:,2*nT+1);
    dydt(:,2) = r(:,3).*Lx'.*(phi-z(:,2)) - (kD + r(:,4)).*z(:,2) - r(:,6*nT-1).*z(:,nT).*z(:,2*nT) + r(:,6*nT).*z(:,2*nT+1);
    dydt(:,3) = r(:,6*nT-1).*z(:,nT).*z(:,2*nT) - r(:,6*nT).*z(:,2*nT+1);
elseif nT == 2
    dydt(:,1) = r(:,1).*Lx'.*(psi-z(:,1)) - (rD + r(:,2)+r(:,3)).*z(:,1) + r(:,4).*z(:,2);
    dydt(:,2) = r(:,3).*z(:,1) - (r(:,4)+r(:,5)).*z(:,2) - r(:,6*nT-1).*z(:,nT).*z(:,2*nT) + r(:,6*nT).*z(:,2*nT+1);
    dydt(:,3) = r(:,6).*Lx'.*(phi-z(:,3)) - (kD + r(:,7)+r(:,8)).*z(:,3) + r(:,9).*z(:,4);
    dydt(:,4) = r(:,8).*z(:,3) - (r(:,9)+r(:,10)).*z(:,4) - r(:,6*nT-1).*z(:,nT).*z(:,2*nT) + r(:,6*nT).*z(:,2*nT+1);
    dydt(:,5) = r(:,6*nT-1).*z(:,nT).*z(:,2*nT) - r(:,6*nT).*z(:,2*nT+1);
else
    k = 2:1:nT-1;
    kint = 3:3:3*nT-3;
    krec = 4:3:3*nT-2;
    kdeg = 5:3:3*nT-1;
    dydt(:,1) = r(:,1).*Lx'.*(psi-z(:,1)) - (rD + r(:,2)+r(:,3)).*z(:,1) + r(:,4).*z(:,2);
    dydt(:,k) = r(:,kint(k-1)).*z(:,k-1) - (r(:,krec(k-1)) + r(:,kdeg(k-1)) + r(:,kint(k))).*z(:,k) + r(:,krec(k)).*z(:,k+1);
    dydt(:,nT) = r(:,3*nT-3).*z(:,nT-1) - (r(:,3*nT-2)+r(:,3*nT-1)).*z(:,nT) - r(:,6*nT-1).*z(:,nT).*z(:,2*nT) + r(:,6*nT).*z(:,2*nT+1);
    kint = 3*nT+2:3:6*nT-4;
    krec = 3*nT+3:3:6*nT-3;
    kdeg = 3*nT+4:3:6*nT-2;
    dydt(:,nT+1) = r(:,3*nT).*Lx'.*(phi-z(:,nT+1)) - (kD + r(:,3*nT+1)+r(:,3*nT+2)).*z(:,nT+1) + r(:,3*nT+3).*z(:,nT+2);
    dydt(:,nT+k) = r(:,kint(k-1)).*z(:,nT+k-1) - (r(:,krec(k-1)) + r(:,kdeg(k-1)) + r(:,kint(k))).*z(:,nT+k) + r(:,krec(k)).*z(:,nT+k+1);
    dydt(:,nT+nT) = r(:,6*nT-4).*z(:,nT+nT-1) - (r(:,6*nT-3)+r(:,6*nT-2)).*z(:,nT+nT) - r(:,6*nT-1).*z(:,nT).*z(:,2*nT) + r(:,6*nT).*z(:,2*nT+1);
    dydt(:,2*nT+1) = r(:,6*nT-1).*z(:,nT).*z(:,2*nT) - r(:,6*nT).*z(:,2*nT+1);
end
dydt = reshape(dydt,[ny*(2*nT+1),1]);

% stop condition: if magnitude of all the elements of dydt vector is less 
% than the set tolerance (set by variable 'tol' defined previously), stop
% the integration scheme
val = sum(sum(abs(dydt) > tol));        % val counts the number of elements in dydt whose 
                                        % magnitude is larger than tol
isterm = 1;                             % isterm defines that the stop condition.
dir = -1;                               % dir defines the direction from which the 
                                        % stop condition is evaluated. In this case, val should 
                                        % be decreasing towards zero 

end