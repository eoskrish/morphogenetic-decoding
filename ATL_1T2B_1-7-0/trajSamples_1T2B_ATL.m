nT = 1; 
nB = 2;
% determine the length of rate and weight vectors
len_rate_vec = 6*nT;
num_wt_params = nT - 1;

param_vector = load(['params_' num2str(nT) 'T' num2str(nB) 'B.txt']);

% extract the rate, receptor profile and feedback parameters
rp = param_vector(1:len_rate_vec);
rD = 1;
kD = 1;
rtp = param_vector(len_rate_vec+1:len_rate_vec+8);
fp = param_vector(len_rate_vec+9:end-num_wt_params);
if nT > 1
    wp = param_vector(end-num_wt_params+1:end);
else 
    wp = 1;
end
% load the previously saved ligand concentration (input) distribution
Lvals = load('Lvals.txt');
top = [1 7 0];

[nx,~,dx,~,nbins] = DeclareNumParams();
ny = 1;

tend = 2^8;
color1 = [0 0.4470 0.7410];
color2 = [0.8500 0.3250 0.0980];
yal = [-1500 -500 -50; 500 250 25; 2000 1000 125];

% Define the stoichiometry matrix 
nu = zeros(6*nT+2,2*nT+1);
for i = 1 : 2*nT+1
    if i ~= nT && i ~= 2*nT+1
        nu([3*i-2 3*i-1 3*i 3*i+1 3*i+2],i) = [1 -1 -1 -1 1];
    end
    if i == nT
        nu([3*i-2 3*i-1 3*i 6*nT+1 6*nT+2],i) = [1 -1 -1 -1 1];
    end
    if i == 2*nT+1
        nu([3*i-2 3*i-1],i) = [1 -1];
    end
end

x = linspace(0,1,nx);
rc = [0 1];
if rc(1) == 0
    psix = rtp(2) + ( rtp(3)*(x.^rtp(1)) )./( (rtp(4)^rtp(1)) + (x.^rtp(1)) ); 
elseif rc(1) == 1
    psix = rtp(2) + ( rtp(3) )./( 1 + ( (x/rtp(4)).^rtp(1)) ) ; 
end
if rc(2) == 0
    phix = rtp(6) + ( rtp(7)*(x.^rtp(5)) )./( (rtp(8)^rtp(5)) + (x.^rtp(5)) ); 
elseif rc(2) == 1
    phix = rtp(6) + ( rtp(7) )./( 1 + ( (x/rtp(8)).^rtp(5)) ) ; 
end

% error tolerances
tol_err = 1e-1;

na = 10;
% nb = 100;     % will be defined later, dependent on the previous time-step
nc = 10;
nd = 100;
delta = 0.05;

% steady state concentration vector for an x_i
SS_X = zeros(ny,2*nT+1);
rate_feedback = zeros(nx,ny);

% variables for later analysis
Rk_sol = zeros(2*nT+1,nx,ny);
theta = zeros(nx,ny);
tic

% choose the appropiate positions 
for ix = 11 : 30: 71
        
    L = mean(Lvals(ix,:));
    psi = psix(ix);
    phi = phix(ix);
    
    % initializing State vector
    R = zeros(1,2*nT+1);
    R(1) = rp(1)*L*psi/(rp(1)*L+rD+rp(2));
    R(nT+1) = rp(3*nT)*L*phi/(rp(3*nT)*L+kD+rp(3*nT+1));
    if nT > 1
       for k = 1:1:nT-1
           R(k+1) = rp(3*k)/rp(3*k+1)*R(k);
           R(nT+k+1) = rp(3*(nT+k)-1)/rp(3*(nT+k))*R(nT+k);
       end
    end
    R(2*nT+1) = rp(6*nT-1)/rp(6*nT)*R(nT)*R(2*nT)/(1+(fp(1)*R(1))^fp(2));
    
    psi = floor(psi);
    phi = floor(phi);
    R = floor(R);

    %initializing Rate vector
    rl = zeros(1,6*nT+2);
    rl(1) = rp(1)*L;
    rl(2) = rD;
    rl(3) = rp(2);
    rl(3*nT+1) = rp(3*nT)*L;
    rl(3*nT+2) = kD;
    rl(3*nT+3) = rp(3*nT+1);
    if nT > 1
        for k = 1:1:nT-1
            rl(3*k+1) = rp(3*k);
            rl(3*k+2) = rp(3*k+1);
            rl(3*k+3) = rp(3*k+2);
            rl(3*(nT+k)+1) = rp(3*(nT+k)-1);
            rl(3*(nT+k)+2) = rp(3*(nT+k));
            rl(3*(nT+k)+3) = rp(3*(nT+k)+1);
        end
    end
    rl(6*nT+1) = rp(6*nT-1);
    rl(6*nT+2) = rp(6*nT);
    
    %change to parfor
    for iter = 1 : ny
        t = 0;
        idx = 1:1:6*nT+2;

        flag_impl = 0;      % this flag variable is 1 if implicit method will be used
        nu_copy = nu;       % storing a copy of the stoichiometry matrix
        
        if nT == 1
            X = 0*R;
        elseif nT == 2 
            X = 0.1*R;
        elseif nT == 3
            X = 0*R;
        end
        
        tmptop = top;
        tmpfp = fp;
        r = rl;              % assign the rate parameters vector into another variable
                             % and declare the unperturbed value of the rate under feedback as r0
        r0 = r(tmptop(2));
        
        tcount = 0;
        tvec = [];
        X1 = [];
        X2 = [];
        rf = [];
        while t < tend
            tcount = tcount + 1;
            
            % update the rate under feedback 
            if tmptop(3) == 0
                r(tmptop(2)) = r0/(1 + (tmpfp(1)*X(tmptop(1)))^tmpfp(2));
            else
                r(tmptop(2)) = r0*(1 + (tmpfp(1)*X(tmptop(1))^tmpfp(3))./(tmpfp(2)^(-tmpfp(3)) + X(tmptop(1))^tmpfp(3)) );
            end
            
            aX = alpha_nT2B(X,r,nT,psi,phi);    % obtain the hazard function/vector
            a0 = sum(aX);                       % sum the hazard function 

            set_alpha = idx(aX > 0);            % all the reactions with alpha_M 
            set_subzero = zeros(1,6*nT+2);
            set_pe = [];                        % set of reactions in partial equilibrium 
            for m = 1 : 6*nT+2
                set_idx = nu_copy(m,:) < 0;
                if min(floor(X(set_idx)./abs(nu(m,set_idx)))) < nc
                    set_subzero(m) = 1;
                end
                if mod(m,3) == 1
                    if abs(aX(m)-aX(m+1)) <= delta*min(aX(m),aX(m+1))
                        set_pe = [set_pe m m+1];
                    end
                end
            end
            set_subzero = idx(logical(set_subzero));             % get the indices of the reactions that may lead to negative numbers of molecules
            set_crit = intersect(set_alpha, set_subzero);        % critical set

            M_expl = setdiff(idx,set_crit);                      % set of indices for explicit tau leaping
            tau_expl = tau_estimator_nT2B(M_expl,nu,X,aX,nT);    % calculate the time for expl tau leaping
            M_impl = setdiff(M_expl,set_pe);                     % set of indices for implicit tau leaping
            tau_impl = tau_estimator_nT2B(M_impl,nu,X,aX,nT);    % calculate the time for impl tau leaping

            if isnan(tau_expl) || isnan(tau_impl)
                disp(['one of tau_exp or tau_impl is NaN at ix =' num2str(ix) ', t = ' num2str(t) ' with flag = ' num2str(flag_impl)])
                break
            end
            % run the SSA (Gillespie algorithm) if both tau_exp and
            % tau_impl do no meet the conditions
            if (isnan(tau_expl) || isnan(tau_impl)) || (tau_expl < na/a0 && tau_impl < na/a0)
                if flag_impl == 1                                % if the previous step was done by impl tau leaping
                    nb = 10;
                else
                    nb = 100;
                end
                for reac_count = 1 : nb 
                    r1 = rand();
                    r2 = rand();
                    aX = alpha_nT2B(X,r,nT,psi,phi);
                    a0 = sum(aX);
                    tau = 1/a0*log(1/r1);
                    a_sum = cumsum(aX);
                    chk_sum = sum(a_sum > r2*a0);
                    reac_id = length(aX)-chk_sum+1;
                    t = t + tau;
                    X = X + nu(reac_id,:);
                    X(1) = min(X(1),psi);
                    X(nT+1) = min(X(nT+1),phi);
                    flag_impl = 0;
                end    
            else
                %else decide whether to run the implicit or the explicit
                %tau leaping scheme
                tau_tilde = exprnd(sum(aX(set_crit)));
                if tau_expl > min(tau_impl/nd,tau_tilde)
                    if tau_tilde > 0
                        tau = min(tau_expl,tau_tilde);
                    else
                        tau = tau_expl;
                    end
                    k = poissrnd(tau*aX);
                    Xnew = X + k*nu;
                    if sum(Xnew<0) > 0
                        tau_expl = tau_expl/2;
                        tau_impl = tau_impl/2;
                    else
                        X = round(Xnew);
                        X(1) = min(X(1),psi);
                        X(nT+1) = min(X(nT+1),phi);
                        t = t + tau;
                        flag_impl = 0;
                    end
                else
                    tau = min(tau_impl,tau_tilde);
                    k = poissrnd(tau*aX);
                    Z = X + (k-tau*aX)*nu;

                    error = 100;
                    Y = X;
                    while norm(error) > tol_err
                        aY = alpha_nT2B(Y,r,nT,psi,phi);
                        G = -Y + Z + tau*aY*nu;
                        Yp = Y + diag(ones(size(Y)));
                        dGdY = zeros(length(Y));
                        for j = 1 : length(Y)
                            aY = alpha_nT2B(Yp(j,:),r,nT,psi,phi);
                            Gnew = -Yp(j,:) + Z + tau*aY*nu;
                            dGdY(:,j) = Gnew-G;
                        end
                        error = transpose(dGdY\G');
                        Y = Y - error;
                    end
                    if sum(Y<0) > 0
                        tau_expl = tau_expl/2;
                        tau_impl = tau_impl/2;
                    else
                        X = round(Y);
                        X(1) = min(X(1),psi);
                        X(nT+1) = min(X(nT+1),phi);
                        t = t + tau;
                        flag_impl = 1;
                    end
                end
            end
            
            if t > 0.5*tend
                if mod(tcount,2^1)==1
                    tvec = [tvec t];
                    X1 = [X1 X(1)];
                    X2 = [X2 X(3)];
                    rf = [rf r(tmptop(2))];
                end
            end

        end
        
        SS_X(iter,:) = X;
        rate_feedback(ix,iter) = r(tmptop(2));
        
        meanX1 = trapz(tvec,X1)/(tvec(end)-tvec(1));
        meanX2 = trapz(tvec,X2)/(tvec(end)-tvec(1));
        
        subplot(2,3,(ix-11)/30+1)
        plot(tvec,X1-meanX1,'-b','LineWidth',2,'Color',color1)
        hold on
        plot(tvec,X2-meanX2,'-r','LineWidth',2,'Color',color2)
        set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
        'XMinorTick', 'on', 'YMinorTick', 'on', 'YGrid', 'on', ...
        'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], ...
        'YTick', yal(1,(ix-11)/30+1):yal(2,(ix-11)/30+1):yal(3,(ix-11)/30+1), ...
        'LineWidth', 1)
    
        hTitle = title(['x = ' num2str((ix-1)*dx)]);
        legend('R^{(1)}','Q^{(1)}')
        hold off
        hxlabel = xlabel('time, 1/r_D');
        hylabel = ylabel('deviation from mean');
        set([hTitle hxlabel hylabel], 'FontName','AvantGarde', 'FontSize', 12, 'FontWeight' , 'bold')
        set(gca, 'FontName','AvantGarde', 'FontSize', 12, 'FontWeight' , 'bold')
        
    end
    hold off
    pause()

    Rk_sol(:,ix,:) = SS_X';
end
toc
