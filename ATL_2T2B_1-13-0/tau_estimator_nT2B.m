function tau_M = tau_estimator_nT2B(M,nu,x,alpha,nT)
    
    eps = 0.04;
    h = ones(1,2*nT+1);
    h([nT 2*nT]) = 2;

    mu_hat = zeros(1,2*nT+1);
    sigma_sq_hat = zeros(1,2*nT+1);
    for i = 1 : 2*nT+1
        mu_hat(i) = alpha(M)*nu(M,i);
        sigma_sq_hat(i) = alpha(M)*(nu(M,i).^2); 
    end
    
    tau_vec = min(max(eps*x./h,1)./abs(mu_hat), (max(eps*x./h,1).^2)./abs(sigma_sq_hat));
    tau_M = min(tau_vec);
end