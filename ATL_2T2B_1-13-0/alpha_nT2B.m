function alpha = alpha_nT2B(x,r,nT,psi,phi)
    
    h = zeros(1,6*nT+2);
    h(1) = psi - x(1);
    h(2) = x(1);
    h(3) = x(1);
    h(3*nT+1) = phi - x(nT+1);
    h(3*nT+2) = x(nT+1);
    h(3*nT+3) = x(nT+1);
    h(6*nT+1) = x(nT)*x(2*nT);
    h(6*nT+2) = x(2*nT+1);
    if nT > 1
        for k = 1:1:nT-1
            h(3*k+1) = x(k);
            h(3*k+2) = x(k+1);
            h(3*k+3) = x(k+1);
            h(3*(nT+k)+1) = x(nT+k);
            h(3*(nT+k)+2) = x(nT+k+1);
            h(3*(nT+k)+3) = x(nT+k+1);
        end
    end 
    
    alpha = h.*r;
    
end