function out = analyticSoln(Ligand,Psi,Phi,rate,feedback,nTiers)

    global top
    global ny
    global nx 
    
    rD = 1;
    kD = 1;
    
    rdn = zeros(1,nTiers);
    rd = rate(2:3:3*nTiers-1);
    rdn(1) = rd(nTiers);
    if nTiers > 1
        for i = 2 : nTiers
            irI = 3*nTiers-3:-3:3;
            irR = 3*nTiers-2:-3:4;
            rdn(i) = rd(nTiers+1-i) + rdn(i-1).*( rate(irI(i-1))./(rate(irR(i-1))+rdn(i-1)) ); 
        end
    end
    
    R = zeros(nx,ny,nTiers);
    R(:,:,1) = rate(1)*Ligand.*repmat(Psi',1,ny)./(rate(1)*Ligand + rD + rdn(nTiers));
    if nTiers > 1
        for i = 2 : nTiers
            irI = 3:3:3*nTiers-3;
            irR = 4:3:3*nTiers-2;
            R(:,:,i) = (rate(irI(i-1))./(rate(irR(i-1))+rdn(nTiers+1-i))) .*R(:,:,i-1) ;
        end
    end
    
    rate_id = top(2);
    if rate_id <= 2 
        rate_id = rate_id;
    elseif rate_id <= 3*nTiers+2
        rate_id = rate_id - 1;
    else
        rate_id = rate_id - 2;
    end
    
    r0 = rate(rate_id);
    r = zeros(nx,ny,6*nTiers);
    for idx = 1 : 6*nTiers
        r(:,:,idx) = rate(idx);
    end
    if top(3) == 0
        r(:,:,rate_id) = r0./(1 + (feedback(1)*R(:,:,top(1))).^feedback(2));
    else
        r(:,:,rate_id) = r0*(1 + (feedback(1)*R(:,:,top(1)).^feedback(3))./(feedback(2)^(-feedback(3)) + R(:,:,top(1)).^feedback(3)) );
    end
    
    kdn = zeros(nx,ny,nTiers);
    kd = zeros(nx,ny,nTiers);
    ideg = 3*nTiers+1:3:6*nTiers-2;
    kd(:,:,1:nTiers) = r(:,:,ideg(1:nTiers));
    kdn(:,:,1) = kd(:,:,nTiers);
    if nTiers > 1
        for i = 2 : nTiers
            ikI = 6*nTiers-4:-3:3*nTiers+2;
            ikR = 6*nTiers-3:-3:3*nTiers+3;
            kdn(:,:,i) = kd(:,:,nTiers+1-i) + kdn(:,:,i-1).*( r(:,:,ikI(i-1))./(r(:,:,ikR(i-1))+kdn(:,:,i-1)) ); 
        end
    end
    
    S = zeros(nx,ny,nTiers);
    S(:,:,1) = r(:,:,3*nTiers).*Ligand.*repmat(Phi',1,ny)./(r(:,:,3*nTiers).*Ligand + kD + kdn(:,:,nTiers));
    if nTiers > 1
        for i = 2 : nTiers
            ikI = 3*nTiers+2:3:6*nTiers-4;
            ikR = 3*nTiers+3:3:6*nTiers-3;
            S(:,:,i) = (r(:,:,ikI(i-1))./(r(:,:,ikR(i-1))+kdn(:,:,nTiers+1-i))) .*S(:,:,i-1);
        end
    end

    out = zeros(nx,ny,2*nTiers+1);
    out(:,:,1:nTiers) = R;
    out(:,:,nTiers+1:2*nTiers) = S;
    out(:,:,2*nTiers+1) = r(:,:,6*nTiers-1)./r(:,:,6*nTiers).*R(:,:,nTiers).*S(:,:,nTiers);
    
end