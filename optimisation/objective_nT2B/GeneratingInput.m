% set the seed for random number generator to the default value, which is 0
% this is done for the purposes of reproducibility
rng('default')

% set input (ligand) distribution parameters
A = 30;             % amplitude of the exponential
lambda = 0.3;       % decay length as a factor of system/tissue/embryo length
[nx,ny,~,~,~] = DeclareNumParams();

x = linspace(0,1,nx);       % positions taken by the cells as relative distance away from the source
mu = A*exp(-x/lambda);      % mean ligand concentration (input) profile in the x-direction: exponential decay
sigma = sqrt(mu);           % profile of the standard deviation of ligand concentration

inputData = cell(nx,2);    
for i = 1: nx
    inputData{i,1} = x(i);                      % store the first entry as position
    list = mu(i) + sigma(i)*randn(1,ny);        % simulating a Gaussian distribution
    listneg = list(list<0);
    listpos = list(list>=0);
    % removing negative concentration values (truncating) and resamping
    % from Gaussian distribution until all entries are positive
    while isempty(listneg) == 0
        list = [listpos mu(i)+sigma(i)*randn(1,length(listneg))];       
        listneg = list(list<0);
        listpos = list(list>=0);
    end
    inputData{i,2} = list;                      % store the second entry as the ligand conc distribution
end

clear list
clear listneg 
clear listpos
clear i
clear nx
clear ny

% saving the workspace used to generate the input distribution and 
% writing the distribution itself as a matrix (Lvals) to a txt file to be used later 
save('InputData.mat')
Lmatrix = cell2mat(inputData);
dlmwrite('Lvals.txt',Lmatrix(:,2:end),'precision',9)