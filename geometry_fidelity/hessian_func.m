function hessian = hessian_func(fun,pt_vec)

% func_pt_vec = fun(pt_vec);

delta = 0.005;
hessian = zeros(length(pt_vec));

eps = [-1 1];

for i_c = 1 : 2
    eps_i = eps(i_c);
    for j_c = 1 : 2
        eps_j = eps(j_c);
        for i = 1 : length(pt_vec)
%             vec_i = log(pt_vec);
            vec_i = pt_vec;
            vec_i(i) = vec_i(i) + eps_i*delta;
%             vec_i = exp(vec_i);
            for j = 1 : length(pt_vec)
                if j == i
                    hessian(i,j) = hessian(i,j) + 0.5*(fun(vec_i) - fun(pt_vec)) / (delta^2);
                else
%                     vec_j = log(pt_vec);
%                     vec_j(j) = vec_j(j) + eps_j*delta;
%                     vec_j = exp(vec_j);

%                     vec_ij = log(vec_i);
                    vec_ij = vec_i;
                    vec_ij(j) = vec_ij(j) + eps_j*delta;
%                     vec_ij = exp(vec_ij);

%             hessian(i,j) = ( (fun(vec_ij)-fun(vec_i)) - (fun(vec_j)-fun(pt_vec)) ) / (delta^2);
                    hessian(i,j) = hessian(i,j) + ( 0.25*eps_i*eps_j *fun(vec_ij) ) / (delta^2);
                end
            end
        end
    end
end

end