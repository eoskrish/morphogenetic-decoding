function percent_changes = grad_func(fun,pt_vec,channel)

% [grad_table,vectors_pos,vectors_neg,func_pt_vec] = grad_func(fun,pt_vec,delta)

func_pt_vec = fun(pt_vec,channel);

vectors = repmat(pt_vec,length(pt_vec),1);

delta_vals = [0.01 0.02 0.05 0.1];
len_dv = length(delta_vals);
delta_func = zeros(length(pt_vec),2*len_dv);
for iter = 1 : len_dv
    delta = delta_vals(iter);
    delta_vec = delta*pt_vec;
    vectors_pos = vectors + diag(delta_vec);
    vectors_neg = vectors - diag(delta_vec);
    for i = 1 : length(pt_vec)
        delta_func(i,iter+len_dv) = (fun(vectors_pos(i,:),channel) - func_pt_vec);
        delta_func(i,len_dv+1-iter) = (fun(vectors_neg(i,:),channel) - func_pt_vec);
    end
end
percent_changes = delta_func./func_pt_vec*100;

% 	vectors_pos(i,vectors_pos(i,:)>ub) = ub(vectors_pos(i,:)>ub);
% 	vectors_neg(i,vectors_neg(i,:)<lb) = lb(vectors_neg(i,:)<lb);

end