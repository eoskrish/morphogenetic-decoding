%% Percent changes in inference error upon variation in parameters

data_dir = 'Opt_1T2B_1-7-0';

minIE = load([data_dir '/' 'min_IEavg_1T2B_AB.txt']);
opt_vec = load([data_dir '/' 'rv_opt_1T2B_AB.txt']);
L = load('Lvals_lambda0pt3.txt');

n_pts = 20;

[sort_minIE,sort_idx] = sort(minIE);

figure(1)
plot(sort_minIE(1:end),'*-')

sort_vec = opt_vec(sort_idx(1:n_pts),:);
channel = [1 2 0 1 1 7 0];
pt_vec = sort_vec(1,:);
percent_changes = grad_func(@IEcalc_2B,pt_vec,channel);

data = percent_changes;
newdata = ones(size(data));
TickVals = [0 0.01 0.1 1 2 5 10 20 50 100 inf];
for tv = 2:length(TickVals)
    ind = data>=TickVals(tv-1) & data<TickVals(tv);
    newdata(ind) = tv-2;
end
imagesc(rescale(newdata))
colormap(parula(10))
hcolorbar = colorbar('Ticks',[0 1 2 3 4 5 6 7 8 9 10]/10,...
    'TickLabels',["0" "0.01" "0.1" "1" "2" "5" "10" "20" "50" "100" ""]);

hcolorbar.Label.Position(1) = 3.5;
ylabel(hcolorbar,'percent change in inference error','interpreter','latex','FontName','AvantGarde','FontSize',12,'Rotation',270);

xaxis = {'-10', '-5', '-2', '-1', '1', '2', '5', '10'};
yaxis = {'r_b','r_d','\kappa_b','\kappa_d','\kappa_C','\kappa_S','a','A_0','A_1','A_2','b','B_0','B_1','B_2','\gamma','n'};

xticks(1:1:20)
xticklabels(xaxis)
yticks(1:1:16)
yticklabels(yaxis)

hxlabel = xlabel('percent variation');
hylabel = ylabel('channel parameters');
set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel],'interpreter','latex','FontName','AvantGarde','FontSize',15)

set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
    'XMinorTick', 'off', 'YMinorTick', 'off', 'YGrid', 'off', ...
    'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], ...
    'LineWidth', 1)


%% import data from the Large set of initial points 

% modify the line below with the correct path to the data folder
data_dir = 'LargeInitOpt_1T2B_1-7-0';   

min_1 = load([data_dir '/' 'min_IEavg_1T2B_AB_1.txt']);
min_2 = load([data_dir '/' 'min_IEavg_1T2B_AB_2.txt']);
min_3 = load([data_dir '/' 'min_IEavg_1T2B_AB_3.txt']);
min_4 = load([data_dir '/' 'min_IEavg_1T2B_AB_4.txt']);
min_IE = [min_1' min_2' min_3' min_4'];

[sort_IE,sort_idx] = sort(min_IE);

opt_1 = load([data_dir '/' 'rv_opt_1T2B_AB_1.txt']);
opt_2 = load([data_dir '/' 'rv_opt_1T2B_AB_2.txt']);
opt_3 = load([data_dir '/' 'rv_opt_1T2B_AB_3.txt']);
opt_4 = load([data_dir '/' 'rv_opt_1T2B_AB_4.txt']);
opt_vec = [opt_1; opt_2; opt_3; opt_4];

sort_vec = opt_vec(sort_idx,:);

cutoff = 0.02;
l_cutoff = length(sort_IE(sort_IE<cutoff));

ch_para_list = {'rA','rd','kA','kd','kC','kS','hcA','A0','A1','A2','hcB','B0','B1','B2','gamma_','n_'};
load('ParameterBounds.mat')

channel = [1 2 0 1 1 7 0];


%% histogram (distributions) of the different channel parameters

P = sort_vec(1:l_cutoff,:);
P(:,9) = P(:,8)+P(:,9);
P(:,13) = P(:,12)+P(:,13);
% labels = {'r_A','r_d','\kappa_A','\kappa_d','\kappa_C','\kappa_S','a','A_0','A_1','A_2','b','B_0','B_1','B_2','\gamma','n'};
labels = {'ligand binding rate, r_b','degradation rate, r_d', ...
          'ligand binding rate, \kappa_b','degradataion rate, \kappa_d', ...
          'conjugation rate, \kappa_C','splitting rate, \kappa_S', ...
          'hill coefficient, a', 'minimum concentration, A_0',...
          'maximum concentration, A_0 + A_1', 'position of half-maximum, A_2',...
          'hill coefficient, b', 'minimum concentration, B_0',...
          'maximum concentration, B_0 + B_1', 'position of half-maximum, B_2',...
          'feedback sensitivity, \gamma','feedback strength, n'};

figure(1)
%use this line for a specific choice of channel parameters
% para_idx = [1 10 15 6 7 16];
para_idx = 1:1:16;

faceColors = {[160 32 240]/255, [30 144 255]/255, [219 0 0]/255};
faceColorMap = [1 1 2 2 3 2 1 1 1 1 2 2 2 2 3 3];
for i = 1 : 16
    subplot(4,4,i)
    if (para_idx(i) == 9) || (para_idx(i) == 13)
        BinEdges = linspace(eval([ch_para_list{para_idx(i)-1} 'min']) + eval([ch_para_list{para_idx(i)} 'min']),...
                   eval([ch_para_list{para_idx(i)-1} 'max']) + eval([ch_para_list{para_idx(i)} 'max']), 11); 
    else
        BinEdges = linspace(eval([ch_para_list{para_idx(i)} 'min']),eval([ch_para_list{para_idx(i)} 'max']),11); 
    end
    h = histogram(P(:,para_idx(i)),BinEdges);
    h.FaceColor = faceColors{faceColorMap(i)};
%     xlim([ eval([ch_para_list{para_idx(i)} 'min']) eval([ch_para_list{para_idx(i)} 'max']) ])
    xlim([ BinEdges(1) BinEdges(end) ]);
    hxlabel = xlabel(labels{para_idx(i)});
    set(gca, 'FontName', 'Helvetica')
    set([hxlabel],'FontName','AvantGarde','FontSize',16)
end


%% Hessian of avg inference error in (normal) parameter-space 

lb = [0.1 0.001 0.1 0.001 0.1 0.1 0 50 0 0.01 0 50 0 0.01 0.01 0];
ub = [1 0.01 1 0.01 1 1 5 250 250 1 5 250 250 1 1 5];

indices = 1:1:l_cutoff;
min_idx = 1;
x0 = sort_vec(min_idx,:);
hessian = hessian_func(@IEcalc_2B_analysis,x0);

[eigvecs,eigvals] = eig(hessian);
[sort_eigvals,sort_eigidx]= sort(diag(eigvals));

new_eigvecs = zeros(size(eigvecs));
for i = 1 : 16
    new_eigvecs(:,i) = eigvecs(:,sort_eigidx(i))/norm(eigvecs(:,sort_eigidx(i)));
end

indices(indices == min_idx) = [];
points = sort_vec(indices,:);
new_points = zeros(size(points));
for i = 1 : l_cutoff-1
    new_points(i,:) = points(i,:)/norm(points(i,:));
end

figure(1)
imagesc(abs(new_points*flip(new_eigvecs,2)))
colormap(brewermap([],"BuPu"))
hcolorbar = colorbar;
hcolorbar.Label.Position(1) = 3.5;
ylabel(hcolorbar,'component along the eigenvector','FontName','AvantGarde','FontSize',12,'Rotation',270);
xticks(1:1:16)

hxlabel = xlabel('Hessian eigenvector index');
hylabel = ylabel('index of the minima (sorted)');
set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel],'interpreter','latex','FontName','AvantGarde','FontSize',15)


figure(2)
diag_eigvals = diag(eigvals);
plot(flip(diag_eigvals(sort_eigidx)),'.k','MarkerSize',10)
xlim([0 17])
xticks(1:1:16)
set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
    'XMinorTick', 'off', 'YMinorTick', 'off', 'YGrid', 'on', 'XGrid', 'on', ...
    'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], 'YTick', 0:20:160, 'XTick', 1:1:16, ...
    'LineWidth', 1)

hxlabel = xlabel('Hessian eigenvector index');
hylabel = ylabel('eigenvalues of Hessian');
set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel],'interpreter','latex','FontName','AvantGarde','FontSize',15)


% optional: components of parameters along the eigenvectors 
% figure(3)
% imagesc(flip(new_eigvecs,2))
% colormap(brewermap([],"BuPu"))
% hcolorbar = colorbar;
% hcolorbar.Label.Position(1) = 3.5;
% ylabel(hcolorbar,'component along the eigenvector','FontSize',12,'Rotation',270);
% xticks(1:1:16)


%% Jacobian of cumulative inference error in (log) parameter-space 

channel = [1 2 0 1 1 7 0];
J = cell(1,l_cutoff);
d = zeros(16,l_cutoff);
V = cell(1,l_cutoff);
for i = 1 : 1 %l_cutoff
    [J{i},d(:,i),V{i}] = J_trans_J(@xstar_2B,sort_vec(i,:),channel);
    if mod(i,25) == 1
        disp(i)
    end
end

% optional: eigenvalues (linear plot)
% figure(1)
% semilogy(1:1:16,abs(flip(d(:,:),1)),'.k','MarkerSize',5)
% hold on
% semilogy(abs(flip(d(:,1),1)),'.r','MarkerSize',18)
% hold off
% xlim([0 17])
% xticks(1:1:16)
% hxlabel = xlabel('Index of the Eigenvector');
% hylabel = ylabel('Eigenvalue');
% set(gca, 'FontName', 'Helvetica')
% set([hxlabel hylabel],'interpreter','latex','FontName','AvantGarde','FontSize',15)

% eigenspectrum: extra columns to give aspect ratio
figure(2)
for i = 1 : 5
    subplot(1,5,i)
    semilogy(linspace(0,1,10),repmat(abs(d(:,i)),1,10),'-r','LineWidth',1.5)
    xticks(2)
    if i == 1
        hylabel = ylabel('eigenvalues of FIM');
    end
    set(gca, 'FontName', 'Helvetica','FontSize',14)
    set([hylabel],'interpreter','latex','FontName','AvantGarde','FontSize',15)
end
set(gca, 'FontName', 'Helvetica','FontSize',14)
set([hylabel],'interpreter','latex','FontName','AvantGarde','FontSize',15)


figure(3)
v = zeros(16,16);
norm_v = v;
for i = 1 : 16
    v(:,i) = V{1}(:,end-i+1);
    norm_v(:,i) = v(:,i)/norm(v(:,1));
end
imagesc(norm_v)
colormap(brewermap([],"BuPu"))
hcolorbar = colorbar;
caxis([-1 1])

yaxis = {'r_b','r_d','\kappa_b','\kappa_d','\kappa_C','\kappa_S','a','A_0','A_1','A_2','b','B_0','B_1','B_2','\gamma','n'};

xticks(1:1:16)
yticks(1:1:16)
yticklabels(yaxis);

hxlabel = xlabel('FIM eigenvector index');
hylabel = ylabel('channel parameter');
set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel],'interpreter','latex','FontName','AvantGarde','FontSize',15)

hcolorbar.Label.Position(1) = 3.5;
ylabel(hcolorbar,'parameter component in the eigenvector','FontSize',12,'Rotation',270);
xticks(1:1:16)
