function [JTJ_matrix,d,V] = J_trans_J(fun,pt_vec,channel)

% [grad_table,vectors_pos,vectors_neg,func_pt_vec] = grad_func(fun,pt_vec,delta)

func_pt_vec = fun(pt_vec,channel);

grad_times_delta = zeros([size(func_pt_vec) length(pt_vec)]);
JTJi = zeros([size(func_pt_vec) length(pt_vec) length(pt_vec)]);

% norm_vals = ones(1,16);
% norm_vals([8 9 12 13]) = 250;
vectors = repmat(pt_vec,length(pt_vec),1);

delta = 0.01;
delta_vec = delta*ones(size(pt_vec));
vectors_pos = exp(log(vectors) + diag(delta_vec));
vectors_neg = exp(log(vectors) - diag(delta_vec));
for i = 1 : length(pt_vec)
%     grad_times_delta(:,:,i) = 0.5*(fun(vectors_pos(i,:).*norm_vals,channel) - func_pt_vec) + 0.5*(fun(vectors_neg(i,:).*norm_vals,channel) - func_pt_vec);
    grad_times_delta(:,:,i) = 0.5*(fun(vectors_pos(i,:),channel) - func_pt_vec) + 0.5*(fun(vectors_neg(i,:),channel) - func_pt_vec);
end
% grad_times_delta = grad_times_delta/delta;

J = zeros(1,length(pt_vec));
for i = 1 : size(func_pt_vec,1)
    for j = 1 : size(func_pt_vec,2)
        J(1,:) = grad_times_delta(i,j,:)/delta;
        JTJi(i,j,:,:) = J' * J;
    end
end
JTJ = sum(sum(JTJi,1),2);


JTJ_matrix = zeros(length(pt_vec));
JTJ_matrix(:,:) = JTJ;


% JTJ_matrix = J' * J; 

[V,D] = eig(JTJ_matrix);
d = diag(D);

% 	vectors_pos(i,vectors_pos(i,:)>ub) = ub(vectors_pos(i,:)>ub);
% 	vectors_neg(i,vectors_neg(i,:)<lb) = lb(vectors_neg(i,:)<lb);

end