function [nx,ny,dx,dxp,nbins] = DeclareNumParams()

    % set all the numerical parameters and return it as an array to be used by Optimisation code

    nx = 101;
    ny = 101;

    dx = 1/(nx-1);
    dxp = 1/nx;

    nbins = 101;
    
end