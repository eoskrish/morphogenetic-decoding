function generate_topologies(nT,nB)

% this function creates all feedback topologies to be scanned for a channel with nB branches and nT tiers
% A feedback topology is reperesented by an array (row vector) with three entries. The first entry stands for the serial 
% number of the  actuator node, the second entry for the serial number of the rate under control and the third to denote 
% positive or negative feedback (0 for negative, 1 for positive). 
% Different topolgies are listed as rows in a Topology-Space matrix, which can be saved as a txt file and used later 
% by other functions or scripts.

% the number of actuator nodes is equal to: (i)  nT,  for nB = 1
%                                           (ii) nT+1, for nB = 2
% because we take it that only the signalling competent species count as
% actuator nodes.
% the number of controlled rates is equal to: (i)  2 nT, for nB = 1
%                                             (ii) 4 nT + 1
if nB == 1
    categories = {'R_to_r'};
    actuators = cell(length(categories),1);
    targets = cell(length(categories),1);
    num_topologies = 0;
    COC = 0;
    
    actuators{1} = [1 nT]; %1 : 1 : nT;
    targets{1} = 3 : 3 : 3*nT;
    targets{1} = sort([targets{1} 4:3:3*nT-2]);
    
    num_topologies = num_topologies + length(actuators{1})*length(targets{1});
    COC =[COC num_topologies];
end

if nB == 2
    categories = {'R_to_r','Q_to_k','R_to_k','Q_to_r'};
    actuators = cell(length(categories),1);
    targets = cell(length(categories),1);
    num_topologies = 0;
    COC = 0;
    
    for i = 1 : length(categories)
        if categories{i}(end) == 'r'  
            if categories{i}(1) == 'R'
                actuators{i} = 1; %1:1:nT;
            else 
                actuators{i} = 2*nT+1;
            end
            targets{i} = 3*nT; %3:3:3*nT;
%             targets{i} = sort([targets{i} 4:3:3*nT-2]);
            if nT > 1
                targets{i} = sort([targets{i} 3*nT-2]);
            end
        else 
            if categories{i}(1) == 'R'
                actuators{i} = 1;
            else 
                actuators{i} = 2*nT+1;
            end
            targets{i} = 6*nT; %3*nT+3:3:6*nT;
%             targets{i} = sort([targets{i} 3*nT+4:3:6*nT+1]);
            if nT > 1
                targets{i} = sort([targets{i} 6*nT-2 6*nT+1]);
            else
                targets{i} = sort([targets{i} 6*nT+1]);
            end
        end 
        num_topologies = num_topologies + length(actuators{i})*length(targets{i});
        COC = [COC num_topologies];
    end
end

topologies = zeros(num_topologies,3);
for idx = 2 : length(COC)
    topologies(COC(idx-1)+1:COC(idx),1:2) = combvec(actuators{idx-1},targets{idx-1})';
end
pos_feedbacks_boolean = mod(topologies(:,2),3)==0;
topologies(pos_feedbacks_boolean,3) = 1;

dlmwrite(['topologies_' num2str(nT) 'T' num2str(nB) 'B.txt'],topologies)

end

