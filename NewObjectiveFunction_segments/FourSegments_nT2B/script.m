data_dir = 'Opt_2T2B_1-13-0';

channel = [2 2 0 1 1 13 0];

figure(1)
hold on

rcomb = {'AA','AB','BA','BB'};

for i = 1 : 4
min_IE = load([data_dir '/' 'min_IEavg_2T2B_' rcomb{i} '.txt']);
sort_IE = sort(min_IE);
figure(1)
plot(sort_IE,'*-')
end
legend(rcomb)
figure(1)
hold off

%%

min_IE = load([data_dir '/' 'min_IEavg_2T2B_AB.txt']);
[sort_IE,sort_idx] = sort(min_IE);

opt_vec = load([data_dir '/' 'rv_opt_2T2B_AB.txt']);
sort_vec = opt_vec(sort_idx,:);

[InfErr,InfErr_x,psix,phix,mean_th,std_th] = IEcalc_2B_analysis(sort_vec(1,:),channel,L);
x = linspace(0,1,101);

% figure(2)
% plot(x,InfErr_x,'*')
% title(['\sigma_X = ' num2str(sort_IE(1))])
% 
% figure(3)
% plot(x,psix)
% hold on
% plot(x,phix)
% hold off
% 
% figure(4)
% errorbar(x,mean(theta,2),std(theta,1,2))

%% import the input distribution

L = load('Lvals_lambda0pt3.txt');

print_flag = 0;
x = linspace(0,1,101);
[nx,ny,~,~] = DeclareNumParams();

%% Section 9.1: Inference Error for nT2B channel
% comments in this section indicate where plotting parameters may need to
% be modified

close all
IEx_min = ones(size(x))/(nx-1);

% POS = minIE_pos;
POS = [2 2];

figure(1)
hIEx2 = plot(x,IEx(:,POS(2)), '.b', 'MarkerSize', 16);
hold on
hLIEx = plot(x,Ligand_IEx, '.k', 'MarkerSize',16);
minIEline = plot(x,IEx_min,'--k','LineWidth',2);
hold off 
ylim([0 0.3])       % ylim parameters may need to be changed 

hxlabel = xlabel('position, $x$ (normalised)');
hylabel = ylabel('local inference error, $\sigma_X$');

set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel],'interpreter','latex','FontName','AvantGarde','FontSize',15)

set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
    'XMinorTick', 'on', 'YMinorTick', 'on', 'YGrid', 'on', ...
    'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], 'YTick', 0:0.05:1, ...
    'LineWidth', 1)

hLegend = legend(['optimised, $\bar{\sigma}_X$ = ' num2str(100*IE(POS(2)),2) '$\%$'],...
                 ['ligand (no processing), $\bar{\sigma}_X$ = ' num2str(100*Ligand_IE,2) '$\%$']);
set(hLegend,'Interpreter','latex');
set(hLegend,'FontSize',12,'Location','NorthWest');

hold off 

if print_flag == 1 
    filename = [input_dir '/' 'IEx_' num2str(nT) 'T' num2str(nB) 'B_NF.svg']; 
    print(gcf,filename,'-dsvg','-r120');   
end

%% Section 9.2: optimised receptor profiles

pos_of_int = [25 50 75];

figure(2)
hpsi2 = plot(x,psix(:,POS(2)),'-b','LineWidth',2);
hold on
hphi2 = plot(x,phix(:,POS(2)),'-r','LineWidth',2);
maxlim = max(max(psix(:,POS(2))),max(phix(:,POS(2))));
rectangle('Position',[x(pos_of_int(1)-2) 50 0.05 maxlim-50], ...
    'FaceColor',[0.4940 0.1840 0.5560 0.3],'EdgeColor','none',...
    'LineWidth',0.1)

rectangle('Position',[x(pos_of_int(2)-2) 50 0.05 maxlim-50], ...
    'FaceColor',[0.6350 0.0780 0.1840 0.3],'EdgeColor','none',...
    'LineWidth',0.1)

rectangle('Position',[x(pos_of_int(3)-2) 50 0.05 maxlim-50], ...
    'FaceColor',[0.4660 0.6740 0.1880 0.3],'EdgeColor','none',...
    'LineWidth',0.1)

hold off

hxlabel = xlabel('position, $x$ (normalised)');
hylabel = ylabel('surface receptor concentration (nM)');

hLegend = legend([hpsi2, hphi2], ...
    'signalling, $\psi$', 'non-signalling, $\phi$', ...
    'Location', 'NorthEast');
set(hLegend,'Interpreter','latex','FontSize',12)

set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel],'interpreter','latex','FontName','AvantGarde','FontSize',15)

% YTick parameters may need to be modified manually
set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
    'XMinorTick', 'on', 'YMinorTick', 'on', 'YGrid', 'off', ...
    'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], 'YTick', 0:50:500, ...
    'LineWidth', 1)

if print_flag == 1
    filename = [input_dir '/' 'receptors_' num2str(nT) 'T' num2str(nB) 'B_NF.svg']; 
    print(gcf,filename,'-dsvg','-r120');   
end

%% Section 9.3: mean output profile with standard deviation 

xconf2 = [x x(end:-1:1)]; 
yconf2 = [mean_th(:,POS(2))-std_th(:,POS(2)); mean_th(end:-1:1,POS(2))+std_th(end:-1:1,POS(2))];

figure(3)
p2 = fill(xconf2,yconf2,'blue');
hold on 
p2.FaceColor = [0.8 0.8 1];      
p2.EdgeColor = 'none';

htheta2 = plot(x,mean_th(:,POS(2)),'-b','LineWidth',2);
hold off
ylim([0 500])

hxlabel = xlabel('position, $x$ (normalised)');
hylabel = ylabel('output concentration, $\theta$ (nM)');

set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel],'interpreter','latex','FontName','AvantGarde','FontSize',15)

% YTick parameters may need to be modified manually
set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
    'XMinorTick', 'on', 'YMinorTick', 'on', 'YGrid', 'on', ...
    'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], 'YTick', 0:100:1000, ...
    'LineWidth', 1)

if print_flag == 1
    filename = [input_dir '/' 'theta_' num2str(nT) 'T' num2str(nB) 'B_NF.svg']; 
    print(gcf,filename,'-dsvg','-r120');  
end

%% Section 9.4: Input-Output relations  

if num_recepcombs == 2
    if POS(2) == 1
        rc = 0;
    elseif POS(2) == 2
        rc = 1;
    end
else
    if POS(2) == 1
        rc = [0 0];
    elseif POS(2) == 2
        rc = [0 1];
    elseif POS(2) == 3
        rc = [1 0];
    elseif POS(2) == 4
        rc = [1 1];
    end
end
channel = [cp rc top];
npts = 100;
maxL = max(max(L));
Laxis = repmat(linspace(0,maxL,npts),[length(pos_of_int),1]);
theta_real = OutputCalc_2B(params,L(pos_of_int,:),pos_of_int,channel);
theta_fn = OutputCalc_2B(params,Laxis,pos_of_int,channel);

figure(4)
plt1 = plot(Laxis(1,:),theta_fn(1,:),'-b');
hold on
plt2 = plot(Laxis(2,:),theta_fn(2,:),'-r');
plt3 = plot(Laxis(3,:),theta_fn(3,:),'-g');

hplt1 = plot(L(pos_of_int(1),:),theta_real(1,:),'ob');
hplt2 = plot(L(pos_of_int(2),:),theta_real(2,:),'or');
hplt3 = plot(L(pos_of_int(3),:),theta_real(3,:),'og');
ylim([0 400])
hold off

set(plt1,'LineStyle', '-', 'LineWidth', 2, 'Color', [0.4940 0.1840 0.5560])
set(hplt1,'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', 8, 'Color', [0.4940 0.1840 0.5560])
set(plt2,'LineStyle', '-', 'LineWidth', 2, 'Color', [0.6350 0.0780 0.1840])
set(hplt2,'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', 8, 'Color', [0.6350 0.0780 0.1840])
set(plt3,'LineStyle', '-', 'LineWidth', 2, 'Color', [0.4660 0.6740 0.1880])
set(hplt3,'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', 8, 'Color', [0.4660 0.6740 0.1880])% set(minIEline, 'LineWidth', 2, 'Color', 'r')

hxlabel = xlabel('input ligand concentration, $L$ (nM)');
hylabel = ylabel('output concentration, $\theta$ (nM)');

text1 = ['$\psi$ = ' num2str(round(psix(pos_of_int(1),POS(2))),3) ' nM, $\phi$ = ' num2str(round(phix(pos_of_int(1),POS(2))),3) ' nM'];
text2 = ['$\psi$ = ' num2str(round(psix(pos_of_int(2),POS(2))),3) ' nM, $\phi$ = ' num2str(round(phix(pos_of_int(2),POS(2))),3) ' nM'];
text3 = ['$\psi$ = ' num2str(round(psix(pos_of_int(3),POS(2))),3) ' nM, $\phi$ = ' num2str(round(phix(pos_of_int(3),POS(2))),3) ' nM'];
hLegend = legend([plt1, plt2, plt3], ...
    text1, text2, text3, ...
    'Interpreter','latex','FontSize',12,'Location','NorthWest');

set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel],'interpreter','latex','FontName','AvantGarde','FontSize',15)

% YTick parameters may need to be modified manually
set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
    'XMinorTick', 'on', 'YMinorTick', 'on', 'YGrid', 'on', ...
    'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], 'YTick', 0:100:700, ...
    'LineWidth', 1)

if print_flag == 1
    filename = [input_dir '/' 'input-output_' num2str(nT) 'T' num2str(nB) 'B_NF.svg']; 
    print(gcf,filename,'-dsvg','-r120');  
end