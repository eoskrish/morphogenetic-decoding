% import the numerical parameters used and bounds on parametrs to be optimised
load('ParameterBounds.mat')

% set number of tiers (nT), branches (nB) and current topology under 
% investigation as global variables
nT = 2;
nB = 2;     % DO NOT change this line
dlmwrite('ChannelArch.txt',[nT nB])

% generate all topologies to be considered and save them in a txt file
% then load the topologies
generate_topologies(nT,nB)
topologies = load(['topologies_' num2str(nT) 'T' num2str(nB) 'B.txt']);

% calculate number of various parameters
num_receptor_params = 4*nB;
num_rate_params = 6*nT+2;
num_wt_params = nT-1;

% if you wish to further restrict the space of feedback topologies to the
% ones that are inter-branch and analytically tractable, uncomment the code
% below. This maybe done, for example, to do a quick check of the 
% Optimisation routine. 
% topologies(topologies(:,2)<=3*nT,:) = [];    % allow only inter-branch feedback
% topologies(topologies(:,1)~=1,:) = [];      % allow only R1 to act as actuator node
% topologies(topologies(:,3)==1,:) = [];      % allow only negative feedbacks

% if you wish check the optimisation results for a particular feedback
% topology, specify the topology here. You will need to uncomment the code
% below. Please use the guidelines in specifying topologies.
topologies = [1 13 0; 1 10 0];            % format: [actuator_node controlled_rate neg(0)/pos(1)]

% overwrite the topologies_nTnB.txt file esp if the search was restricted
writematrix(topologies, ['topologies_' num2str(nT) 'T' num2str(nB) 'B.txt'])

% initialise parallel pool for MATLAB if required
init_pts = 320;                   % number of inital points in optimisation
cores_demanded = 32;             % number of cores demanded from the cluster
if init_pts > 1
    num_threads = min(cores_demanded,init_pts);
    pool = parpool('local',num_threads);
end

for top_idx = 1 : size(topologies,1)
    % load current topology with index top_idx
    top = topologies(top_idx,:);
    dlmwrite(['top_' num2str(nT) 'T' num2str(nB) 'B.txt'],top)
    
    % determine the number of feedback parameters: 
    % 2 if negative, 3 if positive
    num_feedback_params = 2 + topologies(top_idx,3);
    
    % length of the lower (and upper) bound vector 
    len_lb = num_rate_params + num_receptor_params + num_feedback_params;
    
    % assign the lower bound and upper bound vector depending on nT and nB
    lb = zeros(1,len_lb);
    lb(1) = rAmin;
    lb(2) = rD;
    lb(3) = rdmin;
    if nB > 1
        lb(3*nT+1) = kAmin;
        lb(3*nT+2) = kD;
        lb(3*nT+3) = kdmin;
        lb(6*nT+1) = kCmin;
        lb(6*nT+2) = kSmin;
    end
    if nT > 1
        for k = 1:1:nT-1
            lb(3*k+1) = rImin;
            lb(3*k+2) = rRmin;
            lb(3*k+3) = rdmin;
            if nB > 1
                lb(3*(nT+k)+1) = kImin;
                lb(3*(nT+k)+2) = kRmin;
                lb(3*(nT+k)+3) = kdmin;
            end
        end
    end
    if topologies(top_idx,3) == 1
        lb(end-2:end) = [alpha_min gamma_min n_min];
        if nB == 1
            lb(end-6:end-3) = [hcAmin A0min A1min A2min];
        else
            lb(end-10:end-3) = [hcAmin A0min A1min A2min hcAmin A0min A1min A2min];
        end
    else
        lb(end-1:end) = [gamma_min n_min];
        if nB == 1
            lb(end-5:end-2) = [hcAmin A0min A1min A2min];
        else
            lb(end-9:end-2) = [hcAmin A0min A1min A2min hcAmin A0min A1min A2min];
        end
    end
        
    ub = zeros(1,len_lb);
    ub(1) = rAmax;
    ub(2) = rD;
    ub(3) = rdmax;
    if nB > 1
        ub(3*nT+1) = kAmax;
        ub(3*nT+2) = kD;
        ub(3*nT+3) = kdmax;
        ub(6*nT+1) = kCmax;
        ub(6*nT+2) = kSmax;
    end
    if nT > 1
        for k = 1:1:nT-1
            ub(3*k+1) = rImax;
            ub(3*k+2) = rRmax;
            ub(3*k+3) = rdmax;
            if nB > 1
                ub(3*(nT+k)+1) = kImax;
                ub(3*(nT+k)+2) = kRmax;
                ub(3*(nT+k)+3) = kdmax;
            end
        end
    end
    if topologies(top_idx,3) == 1
        ub(end-2:end) = [alpha_max gamma_max n_max];
        if nB == 1
            ub(end-6:end-3) = [hcAmax A0max A1max A2max];
        else
            ub(end-10:end-3) = [hcAmax A0max A1max A2max hcAmax A0max A1max A2max];
        end
    else
        ub(end-1:end) = [gamma_max n_max];
        if nB == 1
            ub(end-5:end-2) = [hcAmax A0max A1max A2max];
        else
            ub(end-9:end-2) = [hcAmax A0max A1max A2max hcAmax A0max A1max A2max];
        end
    end
    
    % remove entries for rD and kD because they will be kept fixed to unity
    % during the optimisation
    if nB > 1
        lb(3*nT+2) = [];
        ub(3*nT+2) = [];
    end
    lb(2) = [];
    ub(2) = [];
    
    % add bounds for the weight parameters
    lb = [lb zeros(1,num_wt_params)];
    ub = [ub ones(1,num_wt_params)];

    % starting optimisation
    len_lb = length(lb);                % update the length of lb and ub vectors
    exitFlag = zeros(init_pts,1);         % variable to store the reason for Optimiser exit
    rv_opt = zeros(init_pts,len_lb);    % create array to store optimised parameter vector
    min_IEavg = zeros(init_pts,1);      % create array to store the min IE achieved 
                                        % starting from different initial points

    % name and create the output directory 
    outdir = ['Opt_' num2str(nT) 'T' num2str(nB) 'B_' num2str(top(1)) '-' num2str(top(2)) '-' num2str(top(3))];
    if ~exist(outdir, 'dir')
       mkdir(outdir)
    end
    
    % ensure that sum of the first nT-1 weights is less than or equal to one during
    % optimisation
    if nT > 2
        A = zeros(1,len_lb);
        A(end-num_wt_params+1:end) = 1;
        b = 1;
    else
        A = [];
        b = [];
    end
    
    % uncomment if you wish to see the patternsearch algorithm in action.
    % do not use when actually runnning an optimisation
    % make sure to remove the ",options" entry in the patternsearch
    % function
%     options = optimoptions('patternsearch','Display','iter','PlotFcn',@psplotbestf);
    
    % The optimisation procedure now runs as follows: 
    % 1. create a certain number of initial points in the parameter space
    % 2. Optimise using PatternSearch algorithm starting with each
    % separately
    % 3. record the minimum avg inference error acheived and the optimised parameter
    % vector that led to it
    
    % setting the seed to default (0) for reproducibility of results.
    rng('default');
    
    % optimisation with AA-type combination (both increasing) receptor profile
    rv_init = lb+(ub-lb).*rand(init_pts,len_lb);
    rc = [0 0];
    dlmwrite('rc.txt', rc)
    % change 'for' to 'parfor' if number of inital points > 1
    parfor idx = 1 : init_pts
        [rv,fval,exitflag,output] = patternsearch(@IEcalc_2B,rv_init(idx,:),A,b,[],[],lb,ub,[]);
        rv_opt(idx,:) = rv;
        min_IEavg(idx) = fval;
        exitFlag(idx) = exitflag;
    end
    dlmwrite([outdir '/' 'rv_init_' num2str(nT) 'T' num2str(nB) 'B_AA.txt'],rv_init,'precision',9)
    dlmwrite([outdir '/' 'rv_opt_' num2str(nT) 'T' num2str(nB) 'B_AA.txt'],rv_opt,'precision',9)
    dlmwrite([outdir '/' 'min_IEavg_' num2str(nT) 'T' num2str(nB) 'B_AA.txt'],min_IEavg,'precision',9)
    dlmwrite([outdir '/' 'exitFlag_' num2str(nT) 'T' num2str(nB) 'B_AA.txt'],exitFlag,'precision',9)
    disp('for/parfor loop for AA combination completed')
    
    % optimisation with AB-type combination (increasing specific, decreasing non-specific) receptor profile
    rv_init = lb+(ub-lb).*rand(init_pts,len_lb);
    rc = [0 1];
    dlmwrite('rc.txt', rc)
    % change 'for' to 'parfor' if number of inital points > 1
    parfor idx = 1 : init_pts
        [rv,fval,exitflag,output] = patternsearch(@IEcalc_2B,rv_init(idx,:),A,b,[],[],lb,ub,[]);
        rv_opt(idx,:) = rv;
        min_IEavg(idx) = fval;
        exitFlag(idx) = exitflag;
    end
    dlmwrite([outdir '/' 'rv_init_' num2str(nT) 'T' num2str(nB) 'B_AB.txt'],rv_init,'precision',9)
    dlmwrite([outdir '/' 'rv_opt_' num2str(nT) 'T' num2str(nB) 'B_AB.txt'],rv_opt,'precision',9)
    dlmwrite([outdir '/' 'min_IEavg_' num2str(nT) 'T' num2str(nB) 'B_AB.txt'],min_IEavg,'precision',9)
    dlmwrite([outdir '/' 'exitFlag_' num2str(nT) 'T' num2str(nB) 'B_AB.txt'],exitFlag,'precision',9)
    disp('for/parfor loop for AB combination completed')
    
    % optimisation with BA-type combination (decreasing specific, increasing non-specific) receptor profile
    rv_init = lb+(ub-lb).*rand(init_pts,len_lb);
    rc = [1 0];
    dlmwrite('rc.txt', rc)
    % change 'for' to 'parfor' if number of inital points > 1
    parfor idx = 1 : init_pts
        [rv,fval,exitflag,output] = patternsearch(@IEcalc_2B,rv_init(idx,:),A,b,[],[],lb,ub,[]);
        rv_opt(idx,:) = rv;
        min_IEavg(idx) = fval;
        exitFlag(idx) = exitflag;
    end
    dlmwrite([outdir '/' 'rv_init_' num2str(nT) 'T' num2str(nB) 'B_BA.txt'],rv_init,'precision',9)
    dlmwrite([outdir '/' 'rv_opt_' num2str(nT) 'T' num2str(nB) 'B_BA.txt'],rv_opt,'precision',9)
    dlmwrite([outdir '/' 'min_IEavg_' num2str(nT) 'T' num2str(nB) 'B_BA.txt'],min_IEavg,'precision',9)
    dlmwrite([outdir '/' 'exitFlag_' num2str(nT) 'T' num2str(nB) 'B_BA.txt'],exitFlag,'precision',9)
    disp('for/parfor loop for BA combination completed')
    
    % optimisation with BB-type combination (both decreasing) receptor profile
    rv_init = lb+(ub-lb).*rand(init_pts,len_lb);
    rc = [1 1];
    dlmwrite('rc.txt', rc)
    % change 'for' to 'parfor' if number of inital points > 1
    parfor idx = 1 : init_pts
        [rv,fval,exitflag,output] = patternsearch(@IEcalc_2B,rv_init(idx,:),A,b,[],[],lb,ub,[]);
        rv_opt(idx,:) = rv;
        min_IEavg(idx) = fval;
        exitFlag(idx) = exitflag;
    end
    dlmwrite([outdir '/' 'rv_init_' num2str(nT) 'T' num2str(nB) 'B_BB.txt'],rv_init,'precision',9)
    dlmwrite([outdir '/' 'rv_opt_' num2str(nT) 'T' num2str(nB) 'B_BB.txt'],rv_opt,'precision',9)
    dlmwrite([outdir '/' 'min_IEavg_' num2str(nT) 'T' num2str(nB) 'B_BB.txt'],min_IEavg,'precision',9)
    dlmwrite([outdir '/' 'exitFlag_' num2str(nT) 'T' num2str(nB) 'B_BB.txt'],exitFlag,'precision',9)
    disp('for/parfor loop for BB combination completed')
    
end
