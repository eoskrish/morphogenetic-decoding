%% This script sets the bounds for all the parameters to be optimised over 
% See Table 1 in the main text of the article for reference

clear all

%% set the bounds for all rate parameters

% we keep the unbinding/desorption rate (rD) fixed and the rest of the parameters are a proportion of rD
rD = 1;

rAmin = 0.01;
rAmax = 0.1;

rdmin = 0.001;
rdmax = 0.01;

rImin = 0.01;
rImax = 0.1;

rRmin = 0.01;
rRmax = 0.1;

kAmin = 0.01;
kAmax = 0.1;

% the unbinding/desorption rate for the second branch (kappa_D abbr as kD) is taken to be equal to the first branch
kD = rD;

kdmin = 0.001;
kdmax = 0.01;

kImin = 0.01;
kImax = 0.1;

kRmin = 0.01;
kRmax = 0.1;

kCmin = 0.1;
kCmax = 1;

kSmin = 0.1;
kSmax = 1;

%% set the bounds for all receptor profile parameters

% parameters for A-type function (monotonically increasing)
A0min = 50;
A0max = 250;

A1min = 0;
A1max = 250;

A2min = 0.01;
A2max = 1;

hcAmin = 0;
hcAmax = 5;

% parameters for B-type function (monotonically decreasing)
B0min = 50;
B0max = 250;

B1min = 0;
B1max = 250;

B2min = 0.01;
B2max = 1;

hcBmin = 0;
hcBmax = 5;

%% set the bounds for feedback parameters

alpha_min = 0;  % min feedback amplification
alpha_max = 9;  % max feedback amplification

gamma_min = 0;  % min feedback sensitivity
gamma_max = 1;  % max feedback sensitivity

n_min = 0;      % min feedback strength
n_max = 5;      % max feedback strength

%% save the workspace to be called later 
save('ParameterBounds.mat')
