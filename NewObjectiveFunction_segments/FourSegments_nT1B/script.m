data_dir = 'Opt_1T1B_1-3-1';

figure(1)
hold on

rcomb = {'A','B'};

for i = 1 : 2
min_IE = load([data_dir '/' 'min_IEavg_1T1B_' rcomb{i} '.txt']);
sort_IE = sort(min_IE);
figure(1)
plot(sort_IE,'*-')
end
legend(rcomb)
figure(1)
hold off

%%

min_IE = load([data_dir '/' 'min_IEavg_1T1B_B.txt']);
[sort_IE,sort_idx] = sort(min_IE);

opt_vec = load([data_dir '/' 'rv_opt_1T1B_B.txt']);
sort_vec = opt_vec(sort_idx,:);

[InfErr,InfErr_x,psix,theta] = IEcalc_analysis(sort_vec(2,:));
x = linspace(0,1,101);

figure(2)
plot(x,InfErr_x)
title(['\sigma_X = ' num2str(sort_IE(2))])

figure(3)
plot(x,psix)

figure(4)
errorbar(x,mean(theta,2),std(theta,1,2))
