function InfErr = IEcalc(param_vector)

% if you wish to check the solutions at every iteration of ix (position index),
% set checks_flag to 1. Not to be done during optimisation.
checks_flag = 0;

% To ensure that all the workers get the same
% assignment of these variables, load them from a previously saved txt file
global nT
global nB
cp = load('ChannelArch.txt');
nT = cp(1);
nB = cp(2);
% determine the length of rate and weight vectors
len_rate_vec = 3*nT-1;
num_wt_params = nT - 1;

% extract the rate, receptor profile and feedback parameters and set them
% as global for each individual kernel
global fp
global rp
rp = param_vector(1:len_rate_vec);
rtp = param_vector(len_rate_vec+1:len_rate_vec+4);
fp = param_vector(len_rate_vec+5:end-num_wt_params);
if nT > 1
    wp = param_vector(end-num_wt_params+1:end);
else 
    wp = 1;
end
% load the previously saved ligand concentration (input) distribution. 
% Change the input filename appropriately, e.g. 'Lvals_lambda0pt3.txt' for lambda = 0.3   
Lvals = load('Lvals_lambda0pt3.txt');

% numerical parameters (constants)
global nx
global ny
global dx
global dxp
[nx,ny,dx,dxp,nbins] = DeclareNumParams();
x = linspace(0,1,nx);

% receptor profiles according to the receptor combination and parameters 
rc = load('rc.txt');
if rc == 0
    psix = rtp(2) + ( rtp(3)*(x.^rtp(1)) )./( (rtp(4)^rtp(1)) + (x.^rtp(1)) ); 
    rf = 'A';
elseif rc == 1
    psix = rtp(2) + ( rtp(3) )./( 1 + ( (x/rtp(4)).^rtp(1)) ) ; 
    rf = 'B';
end

% uncomment below if you wish to check psi profiles. 
% DO NOT uncomment when running an Optimisation! 
% plot(psix)
% pause()
% close all

global psi
global top
global Lx
top = load(['top_' num2str(nT) 'T' num2str(nB) 'B.txt']);

maxt = 2^25;
tspan = [0 maxt];
y0 = zeros(ny,nT);
opts = odeset('Events',@eventfun_nT1B);
SSsoln = zeros(nx,ny,nT);
for ix = 1 : nx
    psi = psix(ix);
    Lx = Lvals(ix,1:ny);
    [t,y] = ode15s(@(t,y) DynEqns_nT1B(t,y),tspan,y0,opts);
    SSsoln(ix,:,:) = reshape(y(end,:),[1,ny,nT]);
    
    % checks to confirm that the scheme works appropriately
    if checks_flag == 1
        for iT = 1 : nT
            y = reshape(y,[size(y,1),ny,nT]);
            figure(iT)
            plot(t,y(:,:,iT))
            xlabel('time, 1/rD units')
            ylabel('concenctrations')
            title([num2str(iT) '-th tier, x = ' num2str(dx*(ix-1))])
        end
        pause()
    end
end
% the output theta is a tier-dependent weighted sum of the concentration of signalling
% species in all the tiers
% defininng the weight matrix
if nT > 1
    w = zeros(nx,ny,nT);
    w(1,1,1:nT-1) = wp;
    w(1,1,nT) = 1-sum(wp);
    for i = 1 : nT
        w(:,:,i) = w(1,1,i);
    end
    theta = sum(w.*SSsoln(:,:,1:nT),3);
else 
    theta = SSsoln(:,:,1);
end

% computing Inference Errors
% mean_th = mean(transpose(theta));
maxtheta = ceil(max(max(theta)));
binEdges = linspace(0,maxtheta,nbins+1);

flat_output = reshape(theta,[1 size(theta,1)*size(theta,2)]);
[histout,~,bins] = histcounts(flat_output,'BinEdges',binEdges,'Normalization','pdf');
prob_th = histout;

prob_th_x = zeros(nx,nbins);
for ix = 1 : nx
   [histout,~,~] = histcounts(theta(ix,:),'BinEdges',binEdges,'Normalization','pdf');
   prob_th_x(ix,:) = histout;    
end

prob_x_th = zeros(nbins,nx);
for ith = 1 : nbins
   if prob_th(ith) == 0 
       prob_x_th(ith,:) = 0;
   else 
       prob_x_th(ith,:) = prob_th_x(:,ith)/prob_th(ith);       
   end
end

bins = reshape(bins,[nx ny]);
xstar = zeros(nx,ny);
xdev = zeros(nx,ny);
grp_x = floor(heaviside(x-0.25)) + floor(heaviside(x-0.5)) + floor(heaviside(x-0.75)); 
for ix = 1 : nx
    for iy = 1 : ny
        [~,pos] = max(prob_x_th(bins(ix,iy),:));
        xstar(ix,iy) = (pos-1)*dx; 
        xdev(ix,iy) = xstar(ix,iy) - x(ix);
    end
end
grp_xstar = floor(heaviside(xstar-0.25)) + floor(heaviside(xstar-0.5)) + floor(heaviside(xstar-0.75));
InfErr_x = sqrt(mean((1-kronDelta(grp_x',grp_xstar)) .* (xdev.^2),2));

InfErr = trapz(x,InfErr_x);

end
