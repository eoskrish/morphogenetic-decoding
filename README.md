# Morphogenetic Decoding 

## About 

MATLAB and Mathematica code files for the work on "Cellular compartmentalisation and receptor promiscuity as a strategy for accurate and robust inference of position during morphogenesis" (https://www.biorxiv.org/content/10.1101/2022.03.30.486187v1)

## Repository organisation

NOTE: you will need to refer the paper to understand the context and details regarding the information below.

The folder 'optimisation' contains the codes with dependencies for channel parameter optimisation (see Optimisation scheme Section 'Performance of a Channel Architecture' in the main text of the paper mentioned above).
The optimisation routines are written separately for 1-branch (Optimisation_nT1B_parallelised.m) and 2-branch (Optimisation_nT2B_parallelised.m) channels. These routines generate output folders with names such as 'Opt_1T2B_1-7-0'. Within these, files like 'min_IEavg____.txt' store the minimum inference error achieved for the feedback topology (e.g. 1-7-0, explained later) and receptor combination (AA/AB/BA/BB, explained in the paper). File like 'rv_opt______.txt' store the channel parameters that minimised the average inference error.

Some sample data for lambda = 0.3 has been provided in folder 'Lambda0pt3'

Folders beginning with 'ATL...' contain scripts and results of Adaptive Tau Leaping algorithms written to simulate the chemical master equations for the case of intrinsic noise.

The folder 'analysis' contains the codes with dependencies for analysing and plotting the obtained results. Further details are in the MATLAB scripts themselves.

Some additional figures in Appendices titled "Robustness, Sensitivity and trade-offs", "Robustness due to Inter-Branch feedback" and "Additional tiers dampen the effects of feedback to provide stability" were made in Mathematica. The file 'Mechanism of Control.nb' in the folder 'analysis' contains the code for these.

## Abbreviations 

ATL -> adaptive tau leaping

1T2B -> 1-tier 2-branch 

1-7-0 -> feedback from node #1 to rate #7 of type 0 (negative) 

1-3-1 -> feedback from node #1 to rate #3 of type 1 (positive) 

see the file 'numbering CRNs.png' for a guide to numbering nodes and rates on a chemical reaction network (CRN). n<sub>T</sub> stands for number of tiers in the channel and k = 1,2,...n<sub>T</sub>-1

## Regarding MATLAB and Mathematica softwares

MATLAB v2021a and Mathematica 13.0 were used with institutional license.

## Installation

You can download a ZIP file of the latest version of this repository or [clone](https://docs.github.com/en/repositories/creating-and-managing-repositories/cloning-a-repository) into it.

## Support & Contributing
If you face issues with the code or you would like to add to the project, please get in touch with me at iyersk@ncbs.res.in

## Author
Krishnan S Iyer 
