function theta = OutputCalc_2B(param_vector,Lvals,xpts,channel)

% if you wish to check the solutions at every iteration of ix (position index),
% set checks_flag to 1
checks_flag = 0;

% set number of tiers and number of branches and global parameters for this
% kernel (parallel worker). To ensure that all the workers get the same
% assignment of these variables, load them from a previously saved txt file
global nT
global nB
% cp = load('ChannelArch.txt');
nT = channel(1); %cp(1);
nB = channel(2); %cp(2);
% determine the length of rate and weight vectors
len_rate_vec = 6*nT;
num_wt_params = nT - 1;

% extract the rate, receptor profile and feedback parameters and set them
% as global for each individual kernel
global fp
global rp
rp = param_vector(1:len_rate_vec);
rtp = param_vector(len_rate_vec+1:len_rate_vec+8);
fp = param_vector(len_rate_vec+9:end-num_wt_params);
% fp = [0 1];
if nT > 1
    wp = param_vector(end-num_wt_params+1:end);
else 
    wp = 1;
end
% load the previously saved ligand concentration (input) distribution
% Lvals = load('Lvals.txt');

% numerical parameters (constants)
global nx
global ny
global dx
global dxp
[nx,~,dx,dxp,~] = DeclareNumParams();
ny = size(Lvals,2);
x = linspace(0,1,nx);
% ny = 1;
nxpts = length(xpts);

% rc = load('rc.txt');
rc = channel(3:4);
if rc(1) == 0
    psix = rtp(2) + ( rtp(3)*(x.^rtp(1)) )./( (rtp(4)^rtp(1)) + (x.^rtp(1)) ); 
elseif rc(1) == 1
    psix = rtp(2) + ( rtp(3) )./( 1 + ( (x/rtp(4)).^rtp(1)) ) ; 
end
if rc(2) == 0
    phix = rtp(6) + ( rtp(7)*(x.^rtp(5)) )./( (rtp(8)^rtp(5)) + (x.^rtp(5)) ); 
elseif rc(2) == 1
    phix = rtp(6) + ( rtp(7) )./( 1 + ( (x/rtp(8)).^rtp(5)) ) ; 
end

% uncomment below if you wish to check phi and psi profiles. DO NOT uncomment when running an Optimisation! 
% plot(psix)
% xlabel('x')
% ylabel('Psi(x), specific (signalling) receptor')
% figure, plot(phix)
% xlabel('x')
% ylabel('Phi(x), promiscuous receptor')
% pause()
% close all

global psi
global phi
global top
global Lx
% top = load(['top_' num2str(nT) 'T' num2str(nB) 'B.txt']);
top = channel(5:7);

% output directory to store errors/warnings 
% outdir = ['Opt_' num2str(nT) 'T' num2str(nB) 'B_' num2str(top(1)) '-' num2str(top(2)) '-' num2str(top(3))];

nx = nxpts;
if top(2) > 3*nT && top(1) <= nT
%     disp([psix(xpts);phix(xpts)])
    SSsoln = analyticSoln(Lvals(1:nxpts,1:ny),psix(xpts),phix(xpts),rp,fp,nT);
else
    maxt = 2^25;
    tspan = [0 maxt];
    y0 = zeros(ny,2*nT+1);
    opts = odeset('Events',@eventfun_nT2B);
    SSsoln = zeros(nx,ny,2*nT+1);
    for ix = 1 : nxpts
        psi = psix(xpts(ix));
        phi = phix(xpts(ix));
        Lx = Lvals(ix,1:ny);
        [t,y] = ode15s(@(t,y) DynEqns_nT2B(t,y),tspan,y0,opts);
        if t(end) == maxt
%             writematrix([param_vector ix],[outdir '/' 'SteadyStateErrorsAA.txt'],'WriteMode','append')
        else 
            checkDyDt = DynEqns_nT2B(t(end),y(end,:));
            if sum(sum(abs(checkDyDt) > 1e-4)) ~= 0
%                 writematrix([param_vector ix],[outdir '/' 'SteadyStateErrorsAA.txt'],'WriteMode','append')
            end
        end
        SSsoln(ix,:,:) = reshape(y(end,:),[1,ny,2*nT+1]);
        
        % checks to confirm that the scheme works appropriately
        if checks_flag == 1
            for iT = 1 : 2*nT+1
                y = reshape(y,[size(y,1),ny,2*nT+1]);
                figure(iT)
                plot(t,y(:,:,iT))
                xlabel('time, 1/rD units')
                ylabel('concenctrations')
                title([num2str(iT) '-th species, x = ' num2str(dx*(ix-1))])
            end
            pause()
        end
    end
end

% the output theta is a tier-dependent weighted sum of the concentration of signalling
% species in all the tiers
% defininng the weight matrix
w = zeros(nx,ny,nT);
w(1,1,1:nT-1) = wp;
w(1,1,nT) = 1-sum(wp);
for i = 1 : nT
    w(:,:,i) = w(1,1,i);
end
if nT > 1
    theta = sum(w.*SSsoln(:,:,1:nT),3) + w(:,:,nT).*SSsoln(:,:,2*nT+1);
else 
    theta = SSsoln(:,:,1) + SSsoln(:,:,2*nT+1);
end

end

