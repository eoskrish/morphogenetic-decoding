function [InfErr,InfErr_x,psix,phix,mean_th,std_th] = IEcalc_2B_optChar(param_vector,channel)

% mean_1T,std_1T,mean_2T,std_2T
% if you wish to check the solutions at every iteration of ix (position index),
% set checks_flag to 1
checks_flag = 0;

% set number of tiers and number of branches and global parameters for this
% kernel (parallel worker). To ensure that all the workers get the same
% assignment of these variables, load them from a previously saved txt file
global nT
global nB
% cp = load('ChannelArch.txt');
nT = channel(1); %cp(1);
nB = channel(2); %cp(2);
% determine the length of rate and weight vectors
len_rate_vec = 6*nT;
num_wt_params = nT - 1;

% extract the rate, receptor profile and feedback parameters and set them
% as global for each individual kernel
global fp
global rp
rp = param_vector(1:len_rate_vec);
rtp = param_vector(len_rate_vec+1:len_rate_vec+8);
fp = param_vector(len_rate_vec+9:end-num_wt_params);
if nT > 1
    wp = param_vector(end-num_wt_params+1:end);
else 
    wp = 1;
end
% load the previously saved ligand concentration (input) distribution
Lvals = load('Lvals_lambda0pt3.txt');

% numerical parameters (constants)
global nx
global ny
global dx
global dxp
[nx,ny,dx,dxp,nbins] = DeclareNumParams();
x = linspace(0,1,nx);

% rc = load('rc.txt');
rc = channel(3:4);
if rc(1) == 0
    psix = rtp(2) + ( rtp(3)*(x.^rtp(1)) )./( (rtp(4)^rtp(1)) + (x.^rtp(1)) ); 
elseif rc(1) == 1
    psix = rtp(2) + ( rtp(3) )./( 1 + ( (x/rtp(4)).^rtp(1)) ) ; 
end
if rc(2) == 0
    phix = rtp(6) + ( rtp(7)*(x.^rtp(5)) )./( (rtp(8)^rtp(5)) + (x.^rtp(5)) ); 
elseif rc(2) == 1
    phix = rtp(6) + ( rtp(7) )./( 1 + ( (x/rtp(8)).^rtp(5)) ) ; 
end

% uncomment below if you wish to check phi and psi profiles. DO NOT uncomment when running an Optimisation! 
% plot(psix)
% xlabel('x')
% ylabel('Psi(x), specific (signalling) receptor')
% figure, plot(phix)
% xlabel('x')
% ylabel('Phi(x), promiscuous receptor')
% pause()
% close all

global psi
global phi
global top
global Lx
% top = load(['top_' num2str(nT) 'T' num2str(nB) 'B.txt']);
top = channel(5:7);

% output directory to store errors/warnings 
outdir = ['Opt_' num2str(nT) 'T' num2str(nB) 'B_' num2str(top(1)) '-' num2str(top(2)) '-' num2str(top(3))];

if top(2) > 3*nT && top(1) <= nT
    SSsoln = analyticSoln(Lvals(1:nx,1:ny),psix,phix,rp,fp,nT);
else
    maxt = 2^25;
    tspan = [0 maxt];
    y0 = zeros(ny,2*nT+1);
    opts = odeset('Events',@eventfun_nT2B);
    SSsoln = zeros(nx,ny,2*nT+1);
    for ix = 1 : nx
        psi = psix(ix);
        phi = phix(ix);
        Lx = Lvals(ix,1:ny);
        [t,y] = ode15s(@(t,y) DynEqns_nT2B(t,y),tspan,y0,opts);
        if t(end) == maxt
%             writematrix([param_vector ix],[outdir '/' 'SteadyStateErrorsAA.txt'],'WriteMode','append')
        else 
            checkDyDt = DynEqns_nT2B(t(end),y(end,:));
            if sum(sum(abs(checkDyDt) > 1e-4)) ~= 0
%                 writematrix([param_vector ix],[outdir '/' 'SteadyStateErrorsAA.txt'],'WriteMode','append')
            end
        end
        SSsoln(ix,:,:) = reshape(y(end,:),[1,ny,2*nT+1]);
        
        % checks to confirm that the scheme works appropriately
        if checks_flag == 1
            for iT = 1 : 2*nT+1
                y = reshape(y,[size(y,1),ny,2*nT+1]);
                figure(iT)
                plot(t,y(:,:,iT))
                xlabel('time, 1/rD units')
                ylabel('concenctrations')
                title([num2str(iT) '-th species, x = ' num2str(dx*(ix-1))])
            end
            pause()
        end
    end
end

% the output theta is a tier-dependent weighted sum of the concentration of signalling
% species in all the tiers
% defininng the weight matrix
w = zeros(nx,ny,nT);
w(1,1,1:nT-1) = wp;
w(1,1,nT) = 1-sum(wp);
for i = 1 : nT
    w(:,:,i) = w(1,1,i);
end
if nT > 1
    theta = sum(w.*SSsoln(:,:,1:nT),3) + w(:,:,nT).*SSsoln(:,:,2*nT+1);
else 
    theta = SSsoln(:,:,1) + SSsoln(:,:,2*nT+1);
end

% analysis
mean_th = mean(transpose(theta));
std_th = std(transpose(theta));
cv_th = std(theta')./mean_th;
cv_L = std(Lvals')./mean(Lvals');
chi = cv_th./cv_L;
maxtheta = ceil(max(max(theta)));
binEdges = linspace(0,maxtheta,nbins+1);
dth = binEdges(2)-binEdges(1);

flat_output = reshape(theta,[1 size(theta,1)*size(theta,2)]);
[histout,~,bins] = histcounts(flat_output,'BinEdges',binEdges,'Normalization','pdf');
prob_th = histout;

prob_th_x = zeros(nx,nbins);
for ix = 1 : nx
   [histout,~,~] = histcounts(theta(ix,:),'BinEdges',binEdges,'Normalization','pdf');
   prob_th_x(ix,:) = histout;    
end

count = 0;
prob_x_th = zeros(nbins,nx);
for ith = 1 : nbins
   if prob_th(ith) == 0 
       prob_x_th(ith,:) = 0;
       count = count + 1;
   else 
       prob_x_th(ith,:) = prob_th_x(:,ith)/prob_th(ith);       
   end
end

bins = reshape(bins,[nx ny]);
xstar = zeros(nx,ny);
xdev = zeros(nx,ny);
for ix = 1 : nx
    for iy = 1 : ny
        [~,pos] = max(prob_x_th(bins(ix,iy),:));
        xstar(ix,iy) = (pos-1)*dx;
        xdev(ix,iy) = xstar(ix,iy) - x(ix);
    end
end
InfErr_x = sqrt(mean(xdev.^2,2));
InfErr = trapz(x,InfErr_x);

% mutual information

MI = 0;
Sg = - sum(dth*prob_th.*log2(prob_th));
S = zeros(nx,1);
for ix = 1 : nx
    for ith = 1 : nbins
        if prob_th_x(ix,ith) ~= 0
            MI = MI + prob_th_x(ix,ith)*log2(prob_th_x(ix,ith)/prob_th(ith));
            S(ix) = S(ix) - dth*prob_th_x(ix,ith)*log2(prob_th_x(ix,ith));
        end
    end
end
MI = MI*dx*dth;
Savg = mean(S);

% tier statistics

% out_1T = zeros(nx,ny);
% out_2T = zeros(nx,ny);
% out_1T(:,:) = SSsoln(:,:,1) + SSsoln(:,:,3);
% out_2T(:,:) = SSsoln(:,:,2) + SSsoln(:,:,4) + SSsoln(:,:,5);
% mean_1T = mean(out_1T,2);
% std_1T = std(out_1T,1,2);
% mean_2T = mean(out_2T,2);
% std_2T = std(out_2T,1,2);

end
