% This code analyses results of the optimisation. Plots in Figures 6,7,8 of 
% the main text and Figures 3,8,14 of the SI have been generated using this 
% code.
%
% Go through the code section by section. Sections are indicated by %%...
% Some sections may need to be run multiple times before plotting. These
% instructions have been indicated in the relevant sections.
%
% To simulate the input, you can do it de novo with Option 1 OR use the 
% same input as for optimisation with Option 2.
% In Option 1, the 'default' seed will give the same input as Option 2 

% outdir = 'D:/GitLab/morphogenetic-decoding/';   % replace with the appropriate output folder you want 
% outdir = 'D:\GitLab\morphogenetic-decoding\NewObjectiveFunction_ segments\FourSegments_nT2B\plots';
outdir = 'D:\GitLab\morphogenetic-decoding\Lambda0pt3\plots';
% outdir = 'C:\Users\kriegerbot\Dropbox\morphogenetic-decoding\optimisation\ThreeGroups_nT2B\plots';
%'D:\GitLab\morphogenetic-decoding\analysis\plots'; %'/home/krishiyer/Dropbox/morphogenetic-decoding/optimisation/ThreeGroups_nT1B/plots';

%% Section 1: simulate the input (Option 1)

% set the seed for random number generator to the default value, which is 0
% this is done for the purposes of reproducibility
rng('default')

% set input (ligand) distribution parameters
A = 30;             % amplitude of the exponential
lambda = 0.3;       % decay length as a factor of system/tissue/embryo length
[nx,ny,~,~,~] = DeclareNumParams();

x = linspace(0,1,nx);       % positions taken by the cells as relative distance away from the source
mu = A*exp(-x/lambda);      % mean ligand concentration (input) profile in the x-direction: exponential decay
sigma = sqrt(mu);           % profile of the standard deviation of ligand concentration

inputData = cell(nx,2);    
for i = 1: nx
    inputData{i,1} = x(i);                      % store the first entry as position
    list = mu(i) + sigma(i)*randn(1,ny);        % simulating a Gaussian distribution
    listneg = list(list<0);
    listpos = list(list>=0);
    % removing negative concentration values (truncating) and resampling
    % from Gaussian distribution until all entries are positive
    while isempty(listneg) == 0
        list = [listpos mu(i)+sigma(i)*randn(1,length(listneg))];       
        listneg = list(list<0);
        listpos = list(list>=0);
    end
    inputData{i,2} = list;                      % store the second entry as the ligand conc distribution
end

clear list
clear listneg 
clear listpos
clear i

% saving the workspace used to generate the input distribution and 
% writing the distribution itself as a matrix (Lvals) to a txt file to be used later 
% save('InputData.mat')
Lmatrix = cell2mat(inputData);
L = Lmatrix(:,2:end);

%% Section 2: simulate the input (Option 2)

% cd('../optimisation')               % replace with the appropriate folder path
L = load('Lvals_lambda0pt3.txt');   % replace with the appropriate filename

%% Section 3: Initialise variables 
% These variables will be collected over multiple iterations of Sections
% below

IEx_nT = zeros(101,4);
xstar_nT = zeros(101,101,4);

chi = zeros(101,4);
xi = zeros(100,4);

minIE_nT = zeros(1,3);
IEx_sigL = zeros(101,3); 

IE_lambda = [];
IEx_lambda = [];
Ligand_IEx_lambda = [];
psix_lambda = [];
phix_lambda = [];
xconf_lambda = [];
yconf_lambda = [];
mean_th_lambda = [];

%% Section 4: declare channel architecture and sift through the folders

clearvars -except minIE_nT IEx_nT xstar_nT chi xi L IEx_sigL IEx_lamL *_lambda outdir

nT = 1;     % number of tiers
nB = 1;     % number of branches

% '../saved-data/Lambda0pt3'
% input_dir = 'D:\GitLab\morphogenetic-decoding\NewObjectiveFunction_ segments\FourSegments_nT2B';
input_dir = 'D:\GitLab\morphogenetic-decoding\Lambda0pt3';
% input_dir = 'C:\Users\kriegerbot\Dropbox\morphogenetic-decoding\optimisation\ThreeGroups_nT2B';
%'D:\GitLab\morphogenetic-decoding\optimisation\ThreeGroups_nT1B'; % '/home/krishiyer/Dropbox/morphogenetic-decoding/optimisation/ThreeGroups_nT1B'; % replace with the appropriate path to folder with results from the optimisation
% The case with no feedback in the channel can be checked by manually setting feedback parameters (variable fp) in IEcalc.m 
% or IEcalc_2B.m to [0 1] or [0 0 1] as appropriate. See IEcalc.m and IEcalc_2B.m for details.  
% It is then recommended to put the results of that optimisation in a
% separate folder (ONLY for the no feedback case).

files = dir([input_dir '/Opt_' num2str(nT) 'T' num2str(nB) 'B*']);
dirFlags = [files.isdir];
subFolders = files(dirFlags);

%% Section 5: construct a table with min IE and the optimised parameters

cd(input_dir)

num_topologies = length(subFolders);
num_recepcombs = 2*nB;

if nB == 1
    receptor_comb = {'A','B'};
elseif nB == 2
    receptor_comb = {'AA','AB','BA','BB'};
end

minIE = zeros(num_topologies,length(receptor_comb));
opt_params = cell(num_topologies,length(receptor_comb));

for i = 1 : num_topologies
    channel_name = subFolders(i).name(5:8);
    for j = 1 : num_recepcombs
        minIE_file = load([subFolders(i).name '/' 'min_IEavg_' channel_name '_' receptor_comb{j} '.txt']);
        opt_params_file = load([subFolders(i).name '/' 'rv_opt_' channel_name '_' receptor_comb{j} '.txt']);
        [val,pos] = min(minIE_file);
        minIE(i,j) = val;
        opt_params{i,j} = opt_params_file(pos,:);
    end
end

Table = table;
topology = cell(num_topologies,1);
for i = 1 : num_topologies
    topology{i} = subFolders(i).name;
end
Table.topology = topology;
Table.minIE = minIE;
Table.OptParams = opt_params;

if num_topologies == 1
    [val,pos2] = min(minIE);
    params = opt_params{pos2};
    pos = [1 pos2];
else
    [val1,pos1] = min(minIE);
    [val,pos2] = min(val1);
    pos = [pos1(pos2) pos2];
    params = opt_params{pos(1),pos(2)};
end
minIE_pos = pos;
minIE_nT(nT) = val;

% uncomnnet the two lines below if you want to specify the Table position
% manually. This is useful if you want to check a particular receptor
% combination (col of Table) within a particular feedback topology (row of Table).
% pos = [3 2];
% params = opt_params{3,2};

%% Section 6: run a loop over receptor combinations in the feedback topology 
% with minimum Inference Error and obtain the relevant channel
% characteristics

% cd('D:\GitLab\non-specific-receptor\matlab-codes\paper-one\analysis')
cd('D:\GitLab\morphogenetic-decoding\analysis')
% cd('C:\Users\kriegerbot\Dropbox\morphogenetic-decoding\optimisation\ThreeGroups_nT2B')
% cd('D:\GitLab\morphogenetic-decoding\optimisation\ThreeGroups_nT1B')
% cd('/home/krishiyer/Dropbox/morphogenetic-decoding/optimisation/ThreeGroups_nT1B')
% uncomment the line below if you wish to save the parameters into txt file
% writematrix(params,[outdir '/' 'params_' num2str(nT) 'T' num2str(nB) 'B.txt'])

[nx,ny,dx,dxp,nbins] = DeclareNumParams();
x = linspace(0,1,nx);
psix = zeros(nx,num_recepcombs);
phix = psix;
IEx = psix;
mean_th = psix;
std_th = psix;
mean_1T = psix;
std_1T = psix;
mean_2T = psix;
std_2T = psix;
xstar = zeros(nx,ny,num_recepcombs);
IE = zeros(1,num_recepcombs);
prob_xstar_x = zeros(nx,101,3);

[Ligand_IE,Ligand_IEx,mean_L,std_L] = LigandIE(L);

for i = 1 : num_recepcombs

    pos(2) = i;

    cp = zeros(1,2);
    cp(1) = str2double(topology{pos(1)}(5));
    cp(2) = str2double(topology{pos(1)}(7));

    top = zeros(1,3);
    idx = regexp(topology{pos(1)},'_');
    tmp = split(topology{pos(1)}(idx(2)+1:end),'-');
    top(1) = str2double(tmp{1});
    top(2) = str2double(tmp{2});
    top(3) = str2double(tmp{3});

    if num_recepcombs == 2
        if pos(2) == 1
            rc = 0;
        elseif pos(2) == 2
            rc = 1;
        end
    else
        if pos(2) == 1
            rc = [0 0];
        elseif pos(2) == 2
            rc = [0 1];
        elseif pos(2) == 3
            rc = [1 0];
        elseif pos(2) == 4
            rc = [1 1];
        end
    end

    channel = [cp rc top];

    % extract the Inference Error and profiles (receptors, mean output, std dev of output)
    % from the architecture with minimum IE
    if nB == 1
        [IE(i),IEx(:,i),psix(:,i),mean_th(:,i),std_th(:,i)] = IEcalc(opt_params{pos(1),pos(2)},channel,L);
%         [IE(i),IEx(:,i),psix(:,i),mean_th(:,i),std_th(:,i)] = IEcalc_analysis(opt_params{pos(1),pos(2)},channel,L);
    elseif nB == 2
%         [IE(i),IEx(:,i),psix(:,i),phix(:,i),mean_th(:,i),std_th(:,i)] = IEcalc_2B(opt_params{pos(1),pos(2)},channel,L);
        [IE(i),IEx(:,i),psix(:,i),phix(:,i),mean_th(:,i),std_th(:,i)] = IEcalc_2B_analysis(opt_params{pos(1),pos(2)},channel,L);
    end

    if i == minIE_pos(2)
        IEx_nT(:,nT) = IEx(:,i);
        xstar_nT(:,:,nT) = xstar(:,:,i);
        binEdges = linspace(0,1.01,102);
        for ix = 1 : nx
           [histout,edges,bin] = histcounts(xstar(ix,:,i),'BinEdges',binEdges,'Normalization','pdf');
           prob_xstar_x(ix,:,nT) = histout;    
        end	
    
        % comment the 2 lines below if you don't want to record chi and xi values.
        % NOTE: nT=1,nB=1 maps to col 1 of chi (and xi)
        % nT=2,nB=1 maps to col 2
        % nT=1,nB=2 maps to col 3
        % nT=2,nB=2 maps to col 4 
        % Please do not record chi and xi for any other nT,nB combinations.
        % This code is not written for that. You will have to alter it
        % appropiately.
        chi(:,nT+2*nB-2) = ( std_th(:,i)./mean_th(:,i) )./( std_L./mean_L);
        xi(:,nT+2*nB-2) = (mean_th(2:end,i)./mean_th(1:end-1,i)-1)./(mean_L(2:end)./mean_L(1:end-1)-1);
    end
    
end

%% Section 7: check the Table and other details

Table
print_flag = 0;     % turn this to 1 to print the figures generated in Section 8 or 9
track_lambda = 0; 

% IEx_sigL(:,1) = IEx(:,2);     %uncomment for tracking sigma (input noise)
% when keeping track of changing sigma (std dev of input), uncomment the
% line above and replace the col numbers with appropriate numbers. Col
% number in IEx represents the receptor combination and Col number in
% IEx_sigL the sequence of sigma=sqrt(0.2*mu), sqrt(0.3*mu), sqrt(0.4*mu).


% from here, 
% Run Section 8.1 to 8.4 for 1-branch channel 
% OR
% Run Section 9.1 to 9.4 for 2-branch channel 
% OR
% Run Section 10 for min Inference Error with no. of tiers 
% OR
% Run Section 11 for Sensitivity vs Robustness plots 
% OR
% Run Sections 8.x (or 9.x) and then Section 12 for channel without
% feedback. More information for this in Section 8.5 (or 9.5)
% OR 
% Run Section 13 for channel properties with changing input characteristics

%% Section 8.1: Inference Error for nT1B channel
% comments in this section indicate where plotting parameters may need to
% be modified

close all
IEx_min = ones(size(x))/(nx-1);

figure(1)
hIEx2 = plot(x,IEx(:,minIE_pos(2)), '.b', 'MarkerSize', 16);
hold on
hLIEx = plot(x,Ligand_IEx, '.k', 'MarkerSize',16);
minIEline = plot(x,IEx_min,'--k','LineWidth',2);
ylim([0 0.3])       % ylim parameters may need to be changed 

hxlabel = xlabel('position, $x$ (normalised)');
hylabel = ylabel('local inference error, $\sigma_X$');

set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel],'interpreter','latex','FontName','AvantGarde','FontSize',15)

% YTick parameters may need to be modified manually
set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
    'XMinorTick', 'on', 'YMinorTick', 'on', 'YGrid', 'on', ...
    'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], 'YTick', 0:0.05:1, ...
    'LineWidth', 1)

hLegend = legend(['optimised, $\bar{\sigma}_X$ = ' num2str(100*IE(2),2) '$\%$'],...
                 ['ligand (no processing), $\bar{\sigma}_X$ = ' num2str(100*Ligand_IE,2) '$\%$']);
set(hLegend,'Interpreter','latex');
set(hLegend,'FontSize',12,'Location','NorthWest');

hold off 

if print_flag == 1
    filename = [outdir '/' 'IEx_' num2str(nT) 'T' num2str(nB) 'B_OldObjFunc.svg']; 
    print(gcf,filename,'-dsvg','-r120')
end

%% Section 8.2: optimised receptor profiles

pos_of_int = [26 51 76];

figure(2)
hpsi2 = plot(x,psix(:,minIE_pos(2)),'-b','LineWidth',2);
hold on

% comment the lines with 'rectangle' if you do not wish to shade specific
% positions (pos_on_int)
rectangle('Position',[x(pos_of_int(1)-2) 50 0.05 max(psix(:,2))-50], ...
    'FaceColor',[0.4940 0.1840 0.5560 0.3],'EdgeColor','none',...
    'LineWidth',0.1)

rectangle('Position',[x(pos_of_int(2)-2) 50 0.05 max(psix(:,2))-50], ...
    'FaceColor',[0.6350 0.0780 0.1840 0.3],'EdgeColor','none',...
    'LineWidth',0.1)

rectangle('Position',[x(pos_of_int(3)-2) 50 0.05 max(psix(:,2))-50], ...
    'FaceColor',[0.4660 0.6740 0.1880 0.3],'EdgeColor','none',...
    'LineWidth',0.1)

hold off

hxlabel = xlabel('position, $x$ (normalised)');
hylabel = ylabel('surface receptor concentration (nM)');

hLegend = legend(hpsi2, ...
    'signalling, $\psi$', ...
    'Location', 'NorthWest');
set(hLegend,'Interpreter','latex','FontSize',12)

set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel],'interpreter','latex','FontName','AvantGarde','FontSize',15)

% YTick parameters may need to be modified manually
set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
    'XMinorTick', 'on', 'YMinorTick', 'on', 'YGrid', 'off', ...
    'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], 'YTick', 0:50:300, ...
    'LineWidth', 1)

if print_flag == 1
    filename = [outdir '/' 'receptors_' num2str(nT) 'T' num2str(nB) 'B.svg']; 
    print(gcf,filename,'-dsvg','-r120');   
end

%% Section 8.3: mean output profile with standard deviation 

xconf2 = [x x(end:-1:1)] ; 
yconf2 = [mean_th(:,minIE_pos(2))-std_th(:,minIE_pos(2)); mean_th(end:-1:1,minIE_pos(2))+std_th(end:-1:1,minIE_pos(2))];

figure(3)
p2 = fill(xconf2,yconf2,'blue');
hold on
p2.FaceColor = [0.8 0.8 1];      
p2.EdgeColor = 'none'; 
htheta2 = plot(x,mean_th(:,2),'-b','LineWidth',2);
hold off

hxlabel = xlabel('position, $x$ (normalised)');
hylabel = ylabel('output concentration, $\theta$ (nM)');

set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel],'interpreter','latex','FontName','AvantGarde','FontSize',15)

% YTick parameters may need to be modified manually
set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
    'XMinorTick', 'on', 'YMinorTick', 'on', 'YGrid', 'on', ...
    'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], 'YTick', 0:50:250, ...
    'LineWidth', 1)

if print_flag == 1
    filename = [outdir '/' 'theta_' num2str(nT) 'T' num2str(nB) 'B.svg']; 
    print(gcf,filename,'-dsvg','-r120');  
end

%% Section 8.4: Input-Output relations  

channel = [cp 1 top];
npts = 100;
maxL = max(max(L));
Laxis = repmat(linspace(0,maxL,npts),[length(pos_of_int),1]);
theta_real = OutputCalc(params,L(pos_of_int,:),pos_of_int,channel);
theta_fn = OutputCalc(params,Laxis,pos_of_int,channel);

figure(4)
hold on
plt1 = plot(Laxis(1,:),theta_fn(1,:),'-');
plt2 = plot(Laxis(2,:),theta_fn(2,:),'-r');
plt3 = plot(Laxis(3,:),theta_fn(3,:),'-g');

hplt1 = plot(L(pos_of_int(1),:),theta_real(1,:),'ob');
hplt2 = plot(L(pos_of_int(2),:),theta_real(2,:),'or');
hplt3 = plot(L(pos_of_int(3),:),theta_real(3,:),'og');

set(plt1,'LineStyle', '-', 'LineWidth', 2, 'Color', [0.4940 0.1840 0.5560])
set(hplt1,'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', 8, 'Color', [0.4940 0.1840 0.5560])
set(plt2,'LineStyle', '-', 'LineWidth', 2, 'Color', [0.6350 0.0780 0.1840])
set(hplt2,'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', 8, 'Color', [0.6350 0.0780 0.1840])
set(plt3,'LineStyle', '-', 'LineWidth', 2, 'Color', [0.4660 0.6740 0.1880])
set(hplt3,'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', 8, 'Color', [0.4660 0.6740 0.1880])

hxlabel = xlabel('input ligand concentration, $L$ (nM)');
hylabel = ylabel('output concentration, $\theta$ (nM)');

text1 = ['$\psi$ = ' num2str(round(psix(pos_of_int(1),2)),3) ' nM'];
text2 = ['$\psi$ = ' num2str(round(psix(pos_of_int(2),2)),3) ' nM'];
text3 = ['$\psi$ = ' num2str(round(psix(pos_of_int(3),2)),3) ' nM'];
hLegend = legend([plt1, plt2, plt3], text1, text2, text3);
set(hLegend,'Interpreter','latex');
set(hLegend,'FontSize',12,'Location','NorthWest');

set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel],'interpreter','latex','FontName','AvantGarde','FontSize',15)

% YTick parameters may need to be modified manually
set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
    'XMinorTick', 'on', 'YMinorTick', 'on', 'YGrid', 'on', ...
    'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], 'YTick', 0:25:250, ...
    'LineWidth', 1)

if print_flag == 1
    filename = [outdir '/' 'input-output_' num2str(nT) 'T' num2str(nB) 'B.svg']; 
    print(gcf,filename,'-dsvg','-r120');  
end

%% Section 8.5: Keeping track of channel properties with changing lambda
% once you're sure Sections 8.1 to 8.4 gave you the correct plots, run this
% section to keep track of channel properties with changing lambda (track_lambda needs to be 1).
% Go to Section 1 to change to the next lambda value and run Sections 3 to 7 again.

if track_lambda == 1
    IE_lambda = [IE_lambda; minIE(POS(1),POS(2))]; 
    IEx_lambda = [IEx_lambda; IEx(:,POS(2))'];
    Ligand_IEx_lambda = [Ligand_IEx_lambda; Ligand_IEx'];
    psix_lambda = [psix_lambda; psix(:,POS(2))'];
    phix_lambda = [phix_lambda; phix(:,POS(2))'];
    xconf_lambda = [xconf_lambda; xconf2];
    yconf_lambda = [yconf_lambda; yconf2'];
    mean_th_lambda = [mean_th_lambda; mean_th(:,POS(2))']; 
end

% Move to section 12 after properties for all lambda values have been collected. 
% Some of the arrays above are not going to be used in Section 12 but no
% harm keeping them for checking anything later

%% Section 9.1: Inference Error for nT2B channel
% comments in this section indicate where plotting parameters may need to
% be modified

close all
IEx_min = ones(size(x))/(nx-1);

POS = minIE_pos;

figure(1)
hIEx2 = plot(x,IEx(:,POS(2)), '.b', 'MarkerSize', 16);
hold on
hLIEx = plot(x,Ligand_IEx, '.k', 'MarkerSize',16);
minIEline = plot(x,IEx_min,'--k','LineWidth',2);
hold off 
ylim([0 0.3])       % ylim parameters may need to be changed 

hxlabel = xlabel('position, $x$ (normalised)');
hylabel = ylabel('local inference error, $\sigma_X$');

set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel],'interpreter','latex','FontName','AvantGarde','FontSize',15)

set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
    'XMinorTick', 'on', 'YMinorTick', 'on', 'YGrid', 'on', ...
    'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], 'YTick', 0:0.05:1, ...
    'LineWidth', 1)

hLegend = legend(['optimised, $\bar{\sigma}_X$ = ' num2str(100*IE(POS(2)),2) '$\%$'],...
                 ['ligand (no processing), $\bar{\sigma}_X$ = ' num2str(100*Ligand_IE,2) '$\%$']);
set(hLegend,'Interpreter','latex');
set(hLegend,'FontSize',12,'Location','NorthWest');

hold off 

if print_flag == 1 
    filename = [outdir '/' 'IEx_' num2str(nT) 'T' num2str(nB) 'B_OldObjFunc.svg']; 
    print(gcf,filename,'-dsvg','-r120');   
end

%% Section 9.2: optimised receptor profiles

pos_of_int = [11 51 81];

figure(2)
hpsi2 = plot(x,psix(:,POS(2)),'-b','LineWidth',2);
hold on
hphi2 = plot(x,phix(:,POS(2)),'-r','LineWidth',2);
maxlim = max(max(psix(:,POS(2))),max(phix(:,POS(2))));

% comment the lines with 'rectangle' if you do not wish to shade specific
% positions (pos_on_int)
rectangle('Position',[x(pos_of_int(1)-2) 50 0.05 maxlim-50], ...
    'FaceColor',[0.4940 0.1840 0.5560 0.3],'EdgeColor','none',...
    'LineWidth',0.1)

rectangle('Position',[x(pos_of_int(2)-2) 50 0.05 maxlim-50], ...
    'FaceColor',[0.6350 0.0780 0.1840 0.3],'EdgeColor','none',...
    'LineWidth',0.1)

rectangle('Position',[x(pos_of_int(3)-2) 50 0.05 maxlim-50], ...
    'FaceColor',[0.4660 0.6740 0.1880 0.3],'EdgeColor','none',...
    'LineWidth',0.1)

hold off

hxlabel = xlabel('position, $x$ (normalised)');
hylabel = ylabel('surface receptor concentration (nM)');

hLegend = legend([hpsi2, hphi2], ...
    'signalling, $\psi$', 'non-signalling, $\phi$', ...
    'Location', 'NorthEast');
set(hLegend,'Interpreter','latex','FontSize',12)

set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel],'interpreter','latex','FontName','AvantGarde','FontSize',15)

% YTick parameters may need to be modified manually
set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
    'XMinorTick', 'on', 'YMinorTick', 'on', 'YGrid', 'off', ...
    'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], 'YTick', 0:50:500, ...
    'LineWidth', 1)

if print_flag == 1
    filename = [outdir '/' 'receptors_' num2str(nT) 'T' num2str(nB) 'B.svg']; 
    print(gcf,filename,'-dsvg','-r120');   
end

%% Section 9.3: mean output profile with standard deviation 

xconf2 = [x x(end:-1:1)]; 
yconf2 = [mean_th(:,POS(2))-std_th(:,POS(2)); mean_th(end:-1:1,POS(2))+std_th(end:-1:1,POS(2))];

figure(3)
p2 = fill(xconf2,yconf2,'blue');
hold on 
p2.FaceColor = [0.8 0.8 1];      
p2.EdgeColor = 'none';

htheta2 = plot(x,mean_th(:,POS(2)),'-b','LineWidth',2);
hold off
ylim([0 1000])

hxlabel = xlabel('position, $x$ (normalised)');
hylabel = ylabel('output concentration, $\theta$ (nM)');

set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel],'interpreter','latex','FontName','AvantGarde','FontSize',15)

% YTick parameters may need to be modified manually
set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
    'XMinorTick', 'on', 'YMinorTick', 'on', 'YGrid', 'on', ...
    'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], 'YTick', 0:200:1000, ...
    'LineWidth', 1)

if print_flag == 1
    filename = [outdir '/' 'theta_' num2str(nT) 'T' num2str(nB) 'B.svg']; 
    print(gcf,filename,'-dsvg','-r120');  
end

%% Section 9.4: Input-Output relations

if num_recepcombs == 2
    if POS(2) == 1
        rc = 0;
    elseif POS(2) == 2
        rc = 1;
    end
else
    if POS(2) == 1
        rc = [0 0];
    elseif POS(2) == 2
        rc = [0 1];
    elseif POS(2) == 3
        rc = [1 0];
    elseif POS(2) == 4
        rc = [1 1];
    end
end
channel = [cp rc top];
npts = 100;
maxL = max(max(L));
Laxis = repmat(linspace(0,maxL,npts),[length(pos_of_int),1]);
theta_real = OutputCalc_2B(params,L(pos_of_int,:),pos_of_int,channel);
theta_fn = OutputCalc_2B(params,Laxis,pos_of_int,channel);

figure(4)
plt1 = plot(Laxis(1,:),theta_fn(1,:),'-b');
hold on
plt2 = plot(Laxis(2,:),theta_fn(2,:),'-r');
plt3 = plot(Laxis(3,:),theta_fn(3,:),'-g');

hplt1 = plot(L(pos_of_int(1),:),theta_real(1,:),'ob');
hplt2 = plot(L(pos_of_int(2),:),theta_real(2,:),'or');
hplt3 = plot(L(pos_of_int(3),:),theta_real(3,:),'og');
ylim([0 900])
hold off

set(plt1,'LineStyle', '-', 'LineWidth', 2, 'Color', [0.4940 0.1840 0.5560])
set(hplt1,'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', 8, 'Color', [0.4940 0.1840 0.5560])
set(plt2,'LineStyle', '-', 'LineWidth', 2, 'Color', [0.6350 0.0780 0.1840])
set(hplt2,'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', 8, 'Color', [0.6350 0.0780 0.1840])
set(plt3,'LineStyle', '-', 'LineWidth', 2, 'Color', [0.4660 0.6740 0.1880])
set(hplt3,'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', 8, 'Color', [0.4660 0.6740 0.1880])% set(minIEline, 'LineWidth', 2, 'Color', 'r')

hxlabel = xlabel('input ligand concentration, $L$ (nM)');
hylabel = ylabel('output concentration, $\theta$ (nM)');

text1 = ['$\psi$ = ' num2str(round(psix(pos_of_int(1),POS(2))),3) ' nM, $\phi$ = ' num2str(round(phix(pos_of_int(1),POS(2))),3) ' nM'];
text2 = ['$\psi$ = ' num2str(round(psix(pos_of_int(2),POS(2))),3) ' nM, $\phi$ = ' num2str(round(phix(pos_of_int(2),POS(2))),3) ' nM'];
text3 = ['$\psi$ = ' num2str(round(psix(pos_of_int(3),POS(2))),3) ' nM, $\phi$ = ' num2str(round(phix(pos_of_int(3),POS(2))),3) ' nM'];
hLegend = legend([plt1, plt2, plt3], ...
    text1, text2, text3, ...
    'Interpreter','latex','FontSize',12,'Location','SouthEast');

set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel],'interpreter','latex','FontName','AvantGarde','FontSize',15)

% YTick parameters may need to be modified manually
set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
    'XMinorTick', 'on', 'YMinorTick', 'on', 'YGrid', 'on', ...
    'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], 'YTick', 0:150:900, ...
    'LineWidth', 1)

if print_flag == 1
    filename = [outdir '/' 'input-output_' num2str(nT) 'T' num2str(nB) 'B.svg']; 
    print(gcf,filename,'-dsvg','-r120');  
end

%% Section 9.5: Keeping track of channel properties with changing lambda
% once you're sure Sections 9.1 to 9.4 gave you the correct plots, run this
% section to keep track of channel properties with changing lambda. Go to
% Section 1 to change to the next lambda value and run Sections 3 to 7 again.

if track_lambda == 1
    IE_lambda = [IE_lambda; minIE(POS(1),POS(2))]; 
    IEx_lambda = [IEx_lambda; IEx(:,POS(2))'];
    Ligand_IEx_lambda = [Ligand_IEx_lambda; Ligand_IEx'];
    psix_lambda = [psix_lambda; psix(:,POS(2))'];
    phix_lambda = [phix_lambda; phix(:,POS(2))'];
    xconf_lambda = [xconf_lambda; xconf2];
    yconf_lambda = [yconf_lambda; yconf2'];
    mean_th_lambda = [mean_th_lambda; mean_th(:,POS(2))']; 
end

% some of the arrays above are not going to be used in Section 12 but no
% harm keeping them for checking anything later

%% Section 10: min Average Inference Errors (AIE) with number of tiers 
% After running Section 1 (or 2) & 3, run the Sections 4 to 6 for different
% values of nT = 1,2,3 with nB fixed. This should store the min AIE 
% achieved in these architectures in the array minIE_nT of size 1x3.

figure()
plt1 = plot(1:1:3,minIE_nT(1:3),'-o','LineWidth',3);
hold on
plot(1:1:3,[0.01 0.01 0.01],'--k','LineWidth',3);

plt1.MarkerFaceColor = [1 0.5 0];
plt1.MarkerSize = 8;
ylim([0 0.025])
hold off

hxlabel = xlabel('number of tiers, $n_T$');
hylabel = ylabel('min avg inference error, $ \bar{\sigma}_X $');

set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel],'Interpreter','latex','FontName','AvantGarde','FontSize',15)

% YTick parameters may need to be modified manually
set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
    'XMinorTick', 'off', 'YMinorTick', 'on', 'XGrid', 'off', 'YGrid', 'on', ...
    'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], 'XTick', 0:1:4, 'YTick', 0:0.005:1, ...
    'LineWidth', 1)

tmp_print_flag = 0;     % to print this figure set the tmp_print_flag variable to 1
if tmp_print_flag == 1
    filename = 'minIE_nT.svg'; 
    print(gcf,filename,'-dsvg','-r120')
end
tmp_print_flag = 0;

%% Section 11: Sensitivity vs Robustness
% this section will utilise the chi and xi values recorded previously. (See NOTE in Section 6) 
% For this, Sections 4 to 6 will need to be run for the following (nT,nB) combinations:
% (1,1), (1,2), (2,1), (2,2)

color1 = [0 0.4470 0.7410];
color2 = [0.8500 0.3250 0.0980];
color3 = [0.9290 0.6940 0.1250];
color4 = [0.4940 0.1840 0.5560];

close all
fig = figure;
subplot(1,4,4)
plt1 = plot(chi(2:end,1),chi(2:end,1)./abs(xi(:,1)),'.','MarkerSize',25,'Color',color1);
xlim([0 1])
ylim([0 3])
legend('1T1B')
subplot(1,4,3)
plt2 = plot(chi(2:end,2),chi(2:end,2)./abs(xi(:,2)),'.','MarkerSize',25,'Color',color2);
xlim([0 1])
ylim([0 3])
legend('2T1B')
subplot(1,4,2)
plt3 = plot(chi(2:end,3),chi(2:end,3)./abs(xi(:,3)),'.','MarkerSize',25,'Color',color3);
xlim([0 1])
ylim([0 3])
legend('1T2B')
subplot(1,4,1)
plt4 = plot(chi(2:end,4),chi(2:end,4)./abs(xi(:,4)),'.','MarkerSize',25,'Color',color4);
xlim([0 1])
ylim([0 3])
legend('2T2B')

han = axes(fig,'visible','off'); 
han.Title.Visible = 'on';
han.XLabel.Visible = 'on';
han.YLabel.Visible = 'on';
hylabel = ylabel(han,'Robustness^{-1} Sensitivity^{-1}, \chi |\xi|^{-1}');
hxlabel = xlabel(han,'Robustness^{-1}, \chi');
hTitle = title(han,'Sensitivity vs Robustness');
set(gca, 'FontName', 'Helvetica')
set([hTitle hxlabel hylabel], 'FontName','AvantGarde', 'FontSize', 12, 'FontWeight' , 'bold')

tmp_print_flag = 0;         % turn this variable to 1 to print this figure
if tmp_print_flag == 1
    filename = 'Sensitivity_vs_Robustness.svg'; 
    print(gcf,filename,'-dsvg','-r120');  
end
tmp_print_flag = 0;

%% Section 12: Inference Error and decay length of mean input (separately optimised channels)
% This Section will need 

close all

color1 = [0 0.4470 0.7410];
color2 = [0.8500 0.3250 0.0980];
color3 = [0.9290 0.6940 0.1250];
color4 = [0.4940 0.1840 0.5560];

IEx_min = 0.5*ones(size(x))/(nx-1);

figure(1)
hIEx1 = plot(x,IEx_lambda(1,:), '.','MarkerSize',16,'Color',color1);
hold on
hIEx2 = plot(x,IEx_lambda(2,:), '.','MarkerSize',16,'Color',color2);
hIEx3 = plot(x,IEx_lambda(3,:), '.','MarkerSize',16,'Color',color3);
minIEline = plot(x,IEx_min,'--k','LineWidth',2);
hold off
ylim([0 0.1])

hxlabel = xlabel('position, $x$ (normalised)');
hylabel = ylabel('local inference error, $\sigma_X$');

set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel],'interpreter','latex','FontName','AvantGarde','FontSize',15)

set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
    'XMinorTick', 'on', 'YMinorTick', 'on', 'YGrid', 'on', ...
    'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], 'YTick', 0:0.01:0.1, ...
    'LineWidth', 1)

hLegend = legend([hIEx1, hIEx2, hIEx3], ...
    ['$\lambda =  0.2, \bar{\sigma}_X$ = ' num2str(100*IE_lambda(1),3) ' $\%$'],...
    ['$\lambda =  0.3, \bar{\sigma}_X$ = ' num2str(100*IE_lambda(2),3) ' $\%$'],...
    ['$\lambda =  0.4, \bar{\sigma}_X$ = ' num2str(100*IE_lambda(3),3) ' $\%$'],...
    'Location', 'NorthWest','Interpreter','latex');
set(hLegend,'FontSize',12)

if print_flag == 1 
    filename = ['IEx_lambda_' num2str(nT) 'T' num2str(nB) 'B.svg']; 
    print(gcf,filename,'-dsvg','-r120');   
end

% 

figure(2)
hpsi1 = plot(x,psix_lambda(1,:),'-','LineWidth',2,'Color',color1);
hold on
hpsi2 = plot(x,psix_lambda(2,:),'-','LineWidth',2,'Color',color2);
hpsi3 = plot(x,psix_lambda(3,:),'-','LineWidth',2,'Color',color3);
hold off

hxlabel = xlabel('position, $x$ (normalised)');
hylabel = ylabel('signalling receptor, $\psi$ (nM)');

hLegend = legend([hpsi1, hpsi2, hpsi3], ...
    '$\lambda =  0.2$', '$\lambda =  0.3$', '$\lambda =  0.4$', ...
    'Location', 'NorthWest','interpreter','latex');
set(hLegend,'FontSize',12)

set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel],'interpreter','latex','FontName','AvantGarde','FontSize',15)

set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
    'XMinorTick', 'on', 'YMinorTick', 'on', 'YGrid', 'on', ...
    'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], 'YTick', 0:50:500, ...
    'LineWidth', 1)

if print_flag == 1
    filename = ['psi_lambda_' num2str(nT) 'T' num2str(nB) 'B.svg']; 
    print(gcf,filename,'-dsvg','-r120');   
end

%

figure(3)
hold on
hphi1 = plot(x,phix_lambda(1,:),'-','LineWidth',2,'Color',color1);
hphi2 = plot(x,phix_lambda(2,:),'-','LineWidth',2,'Color',color2);
hphi3 = plot(x,phix_lambda(3,:),'-','LineWidth',2,'Color',color3);
hold off

hxlabel = xlabel('position, $x$ (normalised)');
hylabel = ylabel('non-signalling receptor, $\phi$ (nM)');

hLegend = legend([hphi1, hphi2, hphi3], ...
    '$\lambda =  0.2$', '$\lambda =  0.3$', '$\lambda =  0.4$', ...
    'Location', 'NorthEast','interpreter','latex');
set(hLegend,'FontSize',12)

set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel],'interpreter','latex','FontName','AvantGarde','FontSize',15)

set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
    'XMinorTick', 'on', 'YMinorTick', 'on', 'YGrid', 'on', ...
    'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], 'YTick', 0:50:500, ...
    'LineWidth', 1)

if print_flag == 1
    filename = ['phi_lambda_' num2str(nT) 'T' num2str(nB) 'B.svg']; 
    print(gcf,filename,'-dsvg','-r120');   
end

%% Section 13: Channel Properties and changing Input Noise
% This will require Sections 1-7 to be run for different profiles of sigma 
% i.e. sigma = sqrt(a*mu) in Section 1 for a = 0.2,0.3,0.4 

nx = 101;
x = linspace(0,1,nx);
IEx_min = ones(size(x))/(nx-1);

POS = minIE_pos;

figure(1)
hIEx1 = plot(x,IEx_sigL(:,1), '.', 'MarkerSize', 16);
hold on
hIEx2 = plot(x,IEx_sigL(:,2), '.', 'MarkerSize', 16);
hIEx3 = plot(x,IEx_sigL(:,3), '.', 'MarkerSize', 16);
minIEline = plot(x,IEx_min,'--k','LineWidth',2);
hold off

hxlabel = xlabel('position, $x$ (normalised)');
hylabel = ylabel('local inference error, $\sigma_X$');

set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel],'interpreter','latex','FontName','AvantGarde','FontSize',15)

set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
    'XMinorTick', 'on', 'YMinorTick', 'on', 'YGrid', 'on', ...
    'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], 'YTick', 0:0.01:0.15, ...
    'LineWidth', 1)

hLegend = legend([hIEx1, hIEx2, hIEx3], ...
    '$\sigma_L = \sqrt{\mu_L}$','$\sigma_L = 0.75 \sqrt{\mu_L}$', ...
    '$\sigma_L = 0.5 \sqrt{\mu_L}$', ...
    'Location', 'NorthWest');
set(hLegend,'interpreter','latex','FontName','AvantGarde', 'FontSize', 12)

tmp_print_flag = 0;     % set this variable to 1 to print the figure
if tmp_print_flag == 1
    filename = 'IEvsInputNoise.svg'; 
    print(gcf,filename,'-dsvg','-r120');  
end
tmp_print_flag = 0;

