%% Response of Inference Error to perturbations in A_2 and B_2 parameters
% execution of this code might take long. Please choose a smaller n_iter for
% a fater run

params = load('params_2T2B.txt');
channel = [2 2 0 1 1 10 0];

A = 30;             % amplitude of the exponential
lambda = 0.3;       % decay length as a factor of system/tissue/embryo length
[nx,ny,~,~,~] = DeclareNumParams();
x = linspace(0,1,nx);       % positions taken by the cells as relative distance away from the source
mu = A*exp(-x/lambda);      % mean ligand concentration (input) profile in the x-direction: exponential decay
sigma = sqrt(mu); 

del1 = 0.01;
del2 = 0.0125;
pert_mat_size = 51;
InfErr_mat = zeros(pert_mat_size);
psi_mat = zeros(pert_mat_size,pert_mat_size,101);
phi_mat = psi_mat;
center_pt = (pert_mat_size+1)/2;
p1 = 16;
p2 = 20;
n_iter = 40;        % this can be reduced for a faster run. It is needed to smoothen the randomness.
InfErr_cum = zeros(pert_mat_size,pert_mat_size,n_iter);

for iter = 1 : n_iter
    
    inputData = cell(nx,2);    
    for i = 1: nx
        inputData{i,1} = x(i);                      % store the first entry as position
        list = mu(i) + sigma(i)*randn(1,ny);        % simulating a Gaussian distribution
        listneg = list(list<0);
        listpos = list(list>=0);
        % removing negative concentration values (truncating) and resampling
        % from Gaussian distribution until all entries are positive
        while isempty(listneg) == 0
            list = [listpos mu(i)+sigma(i)*randn(1,length(listneg))];       
            listneg = list(list<0);
            listpos = list(list>=0);
        end
        inputData{i,2} = list;                      % store the second entry as the ligand conc distribution
    end

    Lmatrix = cell2mat(inputData);
    L = Lmatrix(:,2:end);
    
    for i = 1 : pert_mat_size
        for j = 1 : pert_mat_size
            params_new = [params(1:p1-1) params(p1)+(i-center_pt)*del1 ...
                          params(p1+1:p2-1) params(p2)+(j-center_pt)*del2 params(p2+1:end)];
             [InfErr_mat(i,j),~,psi_mat(i,j,:),phi_mat(i,j,:)] = IEcalc_2B(params_new,channel,L);
        end
    end
    InfErr_cum(:,:,iter) = InfErr_mat;
    
end
InfErr_avg = mean(InfErr_cum,3);

% check contour plot

tmp_print_flag = 0;   % set this to 1 for figures to be saved  

del_psi = zeros(1,pert_mat_size);
del_phi = zeros(1,pert_mat_size);
for i = 1 : pert_mat_size
    del_psi(i) = trapz(x,psi_mat(i,1,:)-psi_mat(26,1,:));
    del_phi(i) = trapz(x,phi_mat(1,i,:)-phi_mat(1,26,:));
end

figure(1)
contourf(del_phi,del_psi,InfErr_avg)
colormap('Jet');
colorbar;
hold on
plot(0,0,'r.','MarkerSize',40)
hold off

% calculate Hessian

spacing = 4;            % this may need to be set manually. Too small or too large a spacing will give erroneous eigendirections
                        % due to the random nature of the function.
elem_vec = center_pt-spacing:spacing:center_pt+spacing;
IE_pert = InfErr_avg(elem_vec,elem_vec);

hessian = zeros(2,2);
hessian(1,1) = (IE_pert(3,2) + IE_pert(1,2) - 2*IE_pert(2,2)) / (del1^2);
hessian(1,2) = (IE_pert(3,3) + IE_pert(2,2) - IE_pert(3,2) - IE_pert(2,3)) / (del1*del2);
hessian(2,1) = (IE_pert(1,1) + IE_pert(2,2) - IE_pert(1,2) - IE_pert(2,1)) / (del1*del2);
hessian(1,2) = (hessian(2,1)+hessian(1,2))/2;
hessian(2,1) = hessian(1,2);
hessian(2,2) = (IE_pert(2,3) + IE_pert(2,1) - 2*IE_pert(2,2)) / (del2^2);

[evec,diag_mat] = eig(hessian);

figure(1)
hold on
ar1 = annotation('arrow');
ar1.Parent = gca;
ar1.Position = [0, 0, 8*evec(1,1), 8*evec(2,1)];
ar1.Color = 'white';
ar1.LineWidth = 3;
ar1.HeadLength = 15;
ar1.HeadWidth = 15;

ar2 = annotation('arrow');
ar2.Parent = gca;
ar2.Position = [0, 0, 8*evec(1,2), 8*evec(2,2)];
ar2.Color = 'white';
ar2.LineWidth = 3;
ar2.HeadLength = 15;
ar2.HeadWidth = 15;
hold off

hxlabel = xlabel('$\Delta \phi$');
hylabel = ylabel('$\Delta \psi$');

set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel],'interpreter','latex','FontName','AvantGarde','FontSize',20)

set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
    'XMinorTick', 'on', 'YMinorTick', 'on', 'XGrid', 'off', 'YGrid', 'off', ...
    'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], 'XTick', -50:10:30, 'YTick', -20:10:40, ...
    'LineWidth', 1)

if tmp_print_flag == 1
    filename = ['InfErr_contours_2T2B.svg']; 
    print(gcf,filename,'-dsvg','-r120');  
end

% demonstrate the receptor profiles upon perturbation

x = linspace(0,1,101);

figure(2)
plot(x,reshape(psi_mat(:,1,:),51,101),'-k','LineWidth',1.25)
hold on 
plot(x,reshape(psi_mat(26,1,:),1,nx),'-b','LineWidth',2)
hold off

hxlabel = xlabel('position, $x$ (normalised)');
hylabel = ylabel('signalling receptor, $\psi$ (nM)');

set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel],'interpreter','latex','FontName','AvantGarde','FontSize',15)

if tmp_print_flag == 1
    filename = ['Perturbations_psi_2T2B.svg']; 
    print(gcf,filename,'-dsvg','-r120');  
end

figure(3)
plot(x,reshape(phi_mat(1,:,:),51,101),'-k','LineWidth',1.25)
hold on 
plot(x,reshape(phi_mat(1,26,:),1,nx),'-b','LineWidth',2)
hold off

hxlabel = xlabel('position, $x$ (normalised)');
hylabel = ylabel('non-signalling receptor, $\phi$ (nM)');

set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel],'interpreter','latex','FontName','AvantGarde','FontSize',15)

if tmp_print_flag == 1
    filename = ['Perturbations_phi_2T2B.svg']; 
    print(gcf,filename,'-dsvg','-r120');  
end
tmp_print_flag = 0;

