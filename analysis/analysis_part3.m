%% Channel properties for uniform & noisy receptor profiles

analysis_dir = 'D:\GitLab\morphogenetic-decoding\analysis';
cd(analysis_dir)

L = load('Lvals_lambda0pt3.txt');
channel = [2 2 0 1 1 10 0];
[Ligand_IE,Ligand_IEx,mean_L,std_L] = LigandIE(L);

nx = 101;
x = linspace(0,1,nx);
IEx_min = ones(size(x))/(nx-1);
IE_vec = zeros(1,3);
IEx_mat = zeros(3,101);
psix_mat = zeros(3,101);
phix_mat = zeros(3,101);

% extract the optimised 2-tier 2-branch channel parameters 
params_opt = load('params_2T2B.txt');

% extract the parameters for 2-tier 2-branch channel optimised keeping
% receptor profile uniform/flat in x
UR_dir = 'D:\GitLab\morphogenetic-decoding\Lambda0pt3\OptUR_2T2B_1-10-0';
minIE_UR = load([UR_dir '\' 'min_IEavg_2T2B.txt']);
[val_UR,pos_UR] = min(minIE_UR); 
rv_opt_UR = load([UR_dir '\' 'rv_opt_2T2B.txt']);
params_UR = rv_opt_UR(pos_UR,:);

% extract the parameters for 2-tier 2-branch channel optimised keeping
% receptor profiles uniform with spatially uncorrelated noise in x
UUcR_dir = 'D:\GitLab\morphogenetic-decoding\Lambda0pt3\OptUUcR_2T2B_1-10-0';
minIE_UUcR = load([UUcR_dir '\' 'min_IEavg_2T2B.txt']);
[val_UUcR,pos_UUcR] = min(minIE_UUcR); 
rv_opt_UUcR = load([UUcR_dir '\' 'rv_opt_2T2B.txt']);
params_UUcR = rv_opt_UUcR(pos_UUcR,:);

% extract the inference errors and receptor profiles
[IE_vec(1),IEx_mat(1,:),psix_mat(1,:),phix_mat(1,:)] = IEcalc_2B(params_opt,channel,L);
[IE_vec(2),IEx_mat(2,:),psix_mat(2,:),phix_mat(2,:)] = IEcalc_2B_UR(params_UR,channel,L);
[IE_vec(3),IEx_mat(3,:),psix_mat(3,:),phix_mat(3,:)] = IEcalc_2B_UUcR(params_UUcR,channel,L);

x = linspace(0,1,101);
tmp_print_flag = 0;     % set this to 1 if you want figures to be saved

figure(1)
hIEx1 = plot(x,IEx_mat(1,:),'.','MarkerSize',16);
hold on
hIEx2 = plot(x,IEx_mat(2,:),'.','MarkerSize',16);
hIEx3 = plot(x,IEx_mat(3,:),'.','MarkerSize',16);
hIEx4 = plot(x,Ligand_IEx,'.k','MarkerSize',16);
minIEline = plot(x,IEx_min,'--k','LineWidth',2);
hold off
ylim([0 0.35])

hxlabel = xlabel('position, $x$');
hylabel = ylabel('local inference error, $\sigma_X$');
hLegend = legend([hIEx1,hIEx2,hIEx3,hIEx4], ...
    ['optimised receptor profiles,' 10 '$\bar{\sigma}_X$ = ' num2str(100*IE_vec(1),2) '$\%$'],...
    ['uniform receptor profiles,' 10 '$\bar{\sigma}_X$ = ' num2str(100*IE_vec(2),2) '$\%$'], ...
    ['uniform receptor profiles' 10 'with uncorrelated noise,' 10 '$\bar{\sigma}_X$ = ' num2str(100*IE_vec(3),2) '$\%$'],...
    ['ligand, with no processing,' 10 '$\bar{\sigma}_X$ = ' num2str(100*Ligand_IE,2) '$\%$']', ...
    'Location', 'NorthEast');
set(hLegend,'interpreter','latex','FontName', 'Helvetica','FontSize',10,'Orientation','horizontal')
hLegend.NumColumns = 2;
set(hLegend,'location','best')

set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel], 'interpreter','latex','FontName','AvantGarde', 'FontSize', 15, 'FontWeight' , 'bold')

set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
    'XMinorTick', 'off', 'YMinorTick', 'on', 'XGrid', 'off', 'YGrid', 'on', ...
    'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], 'XTick', 0:.1:1, 'YTick', 0:0.05:1, ...
    'LineWidth', 1)

if tmp_print_flag == 1
    filename = 'IEx_OFN.svg'; 
    print(gcf,filename,'-dsvg','-r300')
end


figure(2)
hpsix1 = plot(x,psix_mat(1,:),'-','LineWidth',2);
hold on
hpsix2 = plot(x,psix_mat(2,:),'-','LineWidth',2);
hpsix3 = plot(x,psix_mat(3,:),'-','LineWidth',2);
hold off
ylim([0 300])

hxlabel = xlabel('position, $x$');
hylabel = ylabel('signalling receptor, $\psi$ (nM)');
% hTitle = title('Signalling receptor, \psi');

set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel],'interpreter','latex', 'FontName','AvantGarde', 'FontSize', 15, 'FontWeight' , 'bold')

hLegend = legend([hpsix1,hpsix2,hpsix3], ...
    ['optimised profile'], 'uniform profile', ...
    ['uniform profile with uncorrelated noise'], ...
    'Location', 'NorthEast');
set(hLegend,'FontName', 'Helvetica','FontSize',10,'Orientation','horizontal')
hLegend.NumColumns = 2;
set(hLegend,'location','best')

set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
    'XMinorTick', 'off', 'YMinorTick', 'on', 'XGrid', 'off', 'YGrid', 'on', ...
    'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], 'XTick', 0:.1:1, 'YTick', 0:50:500, ...
    'LineWidth', 1)

if tmp_print_flag == 1
    filename = 'psix_OFN.svg'; 
    print(gcf,filename,'-dsvg','-r120')
end

figure(3)
hphix1 = plot(x,phix_mat(1,:),'-','LineWidth',2);
hold on
hphix2 = plot(x,phix_mat(2,:),'-','LineWidth',2);
hphix3 = plot(x,phix_mat(3,:),'-','LineWidth',2);
hold off
ylim([200 550])

hxlabel = xlabel('position, $x$');
hylabel = ylabel('non-signalling receptor, $\phi$ (nM)');
% hTitle = title('Non-signalling receptor, \phi');

set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel],'interpreter','latex', 'FontName','AvantGarde', 'FontSize', 15, 'FontWeight' , 'bold')

hLegend = legend([hphix1,hphix2,hphix3], ...
    ['optimised profile'], 'uniform profile', ...
    ['uniform profile with uncorrelated noise'], ...
    'Location', 'NorthEast');
set(hLegend,'FontName', 'Helvetica','FontSize',10,'Orientation','horizontal')
hLegend.NumColumns = 2;
set(hLegend,'location','best')

set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
    'XMinorTick', 'off', 'YMinorTick', 'on', 'XGrid', 'off', 'YGrid', 'on', ...
    'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], 'XTick', 0:.1:1, 'YTick', 0:50:800, ...
    'LineWidth', 1)

if tmp_print_flag == 1
    filename = 'phix_OFN.svg'; 
    print(gcf,filename,'-dsvg','-r120')
end

tmp_print_flag = 0;
