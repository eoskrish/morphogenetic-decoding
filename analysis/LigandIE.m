function [InfErr,InfErr_x,mean_th,std_th] = LigandIE(Lvals)

theta = Lvals;

[nx,ny,dx,~,nbins] = DeclareNumParams();
x = linspace(0,1,nx);

% analysis
mean_th = mean(theta,2);
std_th = std(theta,0,2);
maxtheta = ceil(max(max(theta)));
binEdges = linspace(0,maxtheta,nbins+1);

flat_output = reshape(theta,[1 size(theta,1)*size(theta,2)]);
[histout,~,bins] = histcounts(flat_output,'BinEdges',binEdges,'Normalization','pdf');
prob_th = histout;

prob_th_x = zeros(nx,nbins);
for ix = 1 : nx
   [histout,~,~] = histcounts(theta(ix,:),'BinEdges',binEdges,'Normalization','pdf');
   prob_th_x(ix,:) = histout;    
end

count = 0;
prob_x_th = zeros(nbins,nx);
for ith = 1 : nbins
   if prob_th(ith) == 0 
       prob_x_th(ith,:) = 0;
       count = count + 1;
   else 
       prob_x_th(ith,:) = prob_th_x(:,ith)/prob_th(ith);       
   end
end

bins = reshape(bins,[nx ny]);
xstar = zeros(nx,ny);
xdev = zeros(nx,ny);
for ix = 1 : nx
    for iy = 1 : ny
        [~,pos] = max(prob_x_th(bins(ix,iy),:));
        xstar(ix,iy) = (pos-1)*dx;
        xdev(ix,iy) = xstar(ix,iy) - x(ix);
    end
end
% InfErr_x = std(xstar,1,2);
InfErr_x = sqrt(mean(xdev.^2,2));
InfErr = trapz(x,InfErr_x);

end