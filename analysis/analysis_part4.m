%% Inference error in presence of intrinsic noise
% for trajectory samples, run the scripts "trajSamples_2T2B_ATL.m"
% in folders ATL_1T2B_1-7-0 and ATL_2T2B_1-13-0

clear
% import the relevant .mat file 
% path to results of the channel with IBF (1-13-0 feedback topology)
input_dir1 = 'D:\GitLab\morphogenetic-decoding\ATL_1T2B_1-7-0';    
load([input_dir1 '/' 'workspace_1T2B.mat'])
IEx_1T2B_IN = InfErr_x;
IE_1T2B = InfErr;

clearvars -except IEx_1T2B_IN IE_1T2B

input_dir2 = 'D:\GitLab\morphogenetic-decoding\ATL_2T2B_1-13-0';    
load([input_dir2 '/' 'workspace_2T2B_1-13-0.mat'])
IEx_2T2B_IN = InfErr_x;
IE_2T2B = InfErr;

IEx_min = ones(size(x))/(nx-1);
figure(1)
hIEx1 = plot(x,IEx_1T2B_IN, '.b', 'MarkerSize', 16);
hold on
hIEx2 = plot(x,IEx_2T2B_IN, '.r', 'MarkerSize', 16);
minIEline = plot(x,IEx_min,'--k','LineWidth',2);
hold off 
ylim([0 0.2])

hxlabel = xlabel('position, $x$ (normalised)');
hylabel = ylabel('local inference error, $\sigma_X$');

set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel],'interpreter','latex','FontName','AvantGarde','FontSize',15)
set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
    'XMinorTick', 'on', 'YMinorTick', 'on', 'YGrid', 'on', ...
    'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], 'YTick', 0:0.05:1, ...
    'LineWidth', 1)

hLegend = legend(['1T2B, $\bar{\sigma}_X$ = ' num2str(100*IE_1T2B,2) '$\%$'],...
                 ['2T2B, $\bar{\sigma}_X$ = ' num2str(100*IE_2T2B,2) '$\%$']);
set(hLegend,'Interpreter','latex');
set(hLegend,'FontSize',12,'Location','NorthEast');

tmp_print_flag = 0;     % set this to 1 to save the figure
if tmp_print_flag == 1 
    filename = [analysis_folder '/' 'IEx_IN.svg']; 
    print(gcf,filename,'-dsvg','-r120');   
end
tmp_print_flag = 0;