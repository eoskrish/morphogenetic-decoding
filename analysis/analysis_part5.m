%% Coefficient of Variation (CV) vs x for the case with the inter-branch feedback (IBF) and with Branch 2 removed (B2R)

% path to results of the channel with IBF
input_dir1 = 'D:\GitLab\morphogenetic-decoding\ATL_2T2B_1-13-0';    
load([input_dir1 '/' 'workspace_2T2B_1-13-0.mat'])

std_th = std(theta,1,2);
mean_th = mean(theta,2);
cv_output_IBF = std_th./mean_th;

% path to results of the channel with B2R
input_dir2 = 'D:\GitLab\morphogenetic-decoding\ATL_2T2B_B2R';
load([input_dir2 '/' 'workspace_2T2B_B2R.mat'])

std_th = std(theta,1,2);
mean_th = mean(theta,2);
cv_output_B2R = std_th./mean_th;

figure(1)
plt1 =  plot(x,cv_output_IBF,'.','MarkerSize',16);
hold on
plt2 = plot(x,cv_output_B2R,'.','MarkerSize',16);
xlim([0 1])
ylim([0 1])
alpha= 0.5;
line([0 1],[1 1],'Color',[0,0,0]+alpha,'LineWidth',1)
line([1 1],[1 0],'Color',[0,0,0]+alpha,'LineWidth',1)
hold off

hxlabel = xlabel('position, $x$', 'interpreter','latex');
hylabel = ylabel('CV');
hTitle = title('Output');

set(gca, 'FontName', 'Times New Roman', 'FontSize',15)
set([hxlabel hylabel], 'FontName','Times New Roman', 'FontSize', 18)
set(hTitle, 'FontName','Times New Roman', 'FontSize', 18, 'FontWeight','normal')

hLegend = legend([plt2,plt1], ...
    'perturbed','optimised', ...
    'Location', 'NorthWest');
set(hLegend,'FontName', 'Times','FontSize',15)

set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
    'XMinorTick', 'off', 'YMinorTick', 'off', 'XGrid', 'off', 'YGrid', 'off', ...
    'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], 'XTick', 0:.1:1, 'YTick', 0:0.1:1, ...
    'LineWidth', 1)

tmp_print_flag = 0;     % set this to 1 to save the figure     
if tmp_print_flag == 1
    filename = 'CV_IBF_B2R_ExtInt.svg'; 
    print(gcf,filename,'-dsvg','-r120')
end
tmp_print_flag = 0;

