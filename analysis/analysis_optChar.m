datafolder = 'D:\GitLab\morphogenetic-decoding\Lambda0pt3';

dir = 'Opt_1T2B_1-7-0';
% cd(dir);

list_min = load([datafolder '/' dir '/' 'min_IEavg_1T2B_AA.txt']);
plot(sort(list_min))
hold on
list_min = load([datafolder '/' dir '/' 'min_IEavg_1T2B_AB.txt']);
plot(sort(list_min))
list_min = load([datafolder '/' dir '/' 'min_IEavg_1T2B_BA.txt']);
plot(sort(list_min))
list_min = load([datafolder '/' dir '/' 'min_IEavg_1T2B_BB.txt']);
plot(sort(list_min))
hold off
legend('AA','AB','BA','BB')

list_min = load([datafolder '/' dir '/' 'min_IEavg_1T2B_AB.txt']);
list_cp = load([datafolder '/' dir '/' 'rv_opt_1T2B_AB.txt']);
[sort_list,sort_idx] = sort(list_min);
sort_list_cp = list_cp(sort_idx,:);

% cd('../')
Lvals = load([datafolder '/' 'Lvals_lambda0pt3.txt']);

n_test = 150;
test_min = zeros(1,n_test);
for i = 1 : n_test
    test_min(i) = IEcalc_2B_optChar(sort_list_cp(i,:),[1 2 0 1 1 7 0]);
end

figure(2)
plot(1:1:n_test,test_min,'o')
hold on
plot(1:1:n_test,sort_list(1:n_test),'.','MarkerSize',10)


k = 2;
C = kmeans(sort_list_cp(1:10,:),k);

[U,S,V] = svd(sort_list_cp(1:10,:));

