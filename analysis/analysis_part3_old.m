%% Channel properties for uniform & noisy receptor profiles

L = load('Lvals_lambda0pt3.txt');
channel = [2 2 0 1 1 10 0];
params = load('params_2T2B.txt');
[Ligand_IE,Ligand_IEx,mean_L,std_L] = LigandIE(L);

IE_vec = zeros(1,3);
IEx_mat = zeros(3,101);
psix_mat = zeros(3,101);
phix_mat = zeros(3,101);

[IE_vec(1),IEx_mat(1,:),psix_mat(1,:),phix_mat(1,:)] = IEcalc_2B(params,channel,L);
[IE_vec(2),IEx_mat(2,:),psix_mat(2,:),phix_mat(2,:)] = IEcalc_2B_flatReceptor(params,channel,L);
[IE_vec(3),IEx_mat(3,:),psix_mat(3,:),phix_mat(3,:)] = IEcalc_2B_noisyReceptor(params,channel,L);

x = linspace(0,1,101);
tmp_print_flag = 0;     % set this to 1 if you want figures to be saved

figure(1)
hIEx1 = plot(x,IEx_mat(1,:),'.','MarkerSize',16);
hold on
hIEx2 = plot(x,IEx_mat(2,:),'.','MarkerSize',16);
hIEx3 = plot(x,IEx_mat(3,:),'.','MarkerSize',16);
hIEx4 = plot(x,Ligand_IEx,'.k','MarkerSize',16);
hold off

hxlabel = xlabel('position, x');
hylabel = ylabel('local inference error');
hLegend = legend([hIEx1,hIEx2,hIEx3,hIEx4], ...
    'optimised receptor profile', 'uniform receptor profile', ...
    ['uniform receptor profile' 10 'with uncorrelated noise'], 'ligand, with no processing', ...
    'Location', 'NorthEast');
set(hLegend,'FontName', 'Helvetica','FontSize',10)

set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel], 'FontName','AvantGarde', 'FontSize', 12, 'FontWeight' , 'bold')

set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
    'XMinorTick', 'off', 'YMinorTick', 'on', 'XGrid', 'off', 'YGrid', 'on', ...
    'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], 'XTick', 0:.1:1, 'YTick', 0:0.1:1, ...
    'LineWidth', 1)

if tmp_print_flag == 1
    filename = 'IEx_OFN.svg'; 
    print(gcf,filename,'-dsvg','-r300')
end

figure(2)
hpsix1 = plot(x,psix_mat(1,:),'-','LineWidth',2);
hold on
hpsix2 = plot(x,psix_mat(2,:),'-','LineWidth',2);
hpsix3 = plot(x,psix_mat(3,:),'-','LineWidth',2);
hold off

hxlabel = xlabel('position, x');
hylabel = ylabel('concentration (nM)');
hTitle = title('Signalling receptor, \psi');

set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel hTitle], 'FontName','AvantGarde', 'FontSize', 12, 'FontWeight' , 'bold')

hLegend = legend([hpsix1,hpsix2,hpsix3], ...
    'optimised receptor profile', 'uniform receptor profile', ...
    ['uniform receptor profile' 10 'with uncorrelated noise'], ...
    'Location', 'NorthEast');
set(hLegend,'FontName', 'Helvetica','FontSize',10)

set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
    'XMinorTick', 'off', 'YMinorTick', 'on', 'XGrid', 'off', 'YGrid', 'on', ...
    'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], 'XTick', 0:.1:1, 'YTick', 0:50:500, ...
    'LineWidth', 1)

if tmp_print_flag == 1
    filename = 'psix_OFN.svg'; 
    print(gcf,filename,'-dsvg','-r120')
end

figure(3)
hphix1 = plot(x,phix_mat(1,:),'-','LineWidth',2);
hold on
hphix2 = plot(x,phix_mat(2,:),'-','LineWidth',2);
hphix3 = plot(x,phix_mat(3,:),'-','LineWidth',2);
hold off

hxlabel = xlabel('position, x');
hylabel = ylabel('concentration (nM)');
hTitle = title('Non-signalling receptor, \phi');

set(gca, 'FontName', 'Helvetica')
set([hxlabel hylabel hTitle], 'FontName','AvantGarde', 'FontSize', 12, 'FontWeight' , 'bold')

hLegend = legend([hphix1,hphix2,hphix3], ...
    'optimised receptor profile', 'uniform receptor profile', ...
    ['uniform receptor profile' 10 'with uncorrelated noise'], ...
    'Location', 'NorthEast');
set(hLegend,'FontName', 'Helvetica','FontSize',10)

set(gca, 'Box', 'off', 'TickDir', 'out', 'TickLength', [.02 .02], ...
    'XMinorTick', 'off', 'YMinorTick', 'on', 'XGrid', 'off', 'YGrid', 'on', ...
    'XColor', [.3 .3 .3], 'YColor', [.3 .3 .3], 'XTick', 0:.1:1, 'YTick', 0:50:800, ...
    'LineWidth', 1)

if tmp_print_flag == 1
    filename = 'phix_OFN.svg'; 
    print(gcf,filename,'-dsvg','-r120')
end

tmp_print_flag = 0;
