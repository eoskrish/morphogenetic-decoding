function [val,isterm,dir] = eventfun_nT1B(t,y)

global nT
global rp
global fp
global Lx
global psi
global top
global ny

tol = 1e-4;

z = reshape(y,[ny,nT]);

r = repmat(rp,[ny,1]);
rate_id = top(2);
if rate_id <= 3*nT
    rate_id = rate_id - 1;
else
    rate_id = rate_id - 2;
end
r0 = rp(rate_id);
if top(3) == 0
    r(:,rate_id) = r0./(1 + (fp(1)*z(:,top(1))).^fp(2));
else
    r(:,rate_id) = r0*(1 + (fp(1)*z(:,top(1)).^fp(3))./(fp(2)^(-fp(3)) + z(:,top(1)).^fp(3)) );
end

rD = 1;
if nT == 1
    dydt(:,1) = r(:,1).*Lx'.*(psi-z(:,1)) - (rD + r(:,2)).*z(:,1);
elseif nT == 2
    dydt(:,1) = r(:,1).*Lx'.*(psi-z(:,1)) - (rD + r(:,2)+r(:,3)).*z(:,1) + r(:,4).*z(:,2);
    dydt(:,2) = r(:,3).*z(:,1) - (r(:,4)+r(:,5)).*z(:,2);
else
    k = 2:1:nT-1;
    kint = 3:3:3*nT-3;
    krec = 4:3:3*nT-2;
    kdeg = 5:3:3*nT-1;
    dydt(:,1) = r(:,1).*Lx'.*(psi-z(:,1)) - (rD + r(:,2)+r(:,3)).*z(:,1) + r(:,4).*z(:,2);
    dydt(:,k) = r(:,kint(k-1)).*z(:,k-1) - (r(:,krec(k-1)) + r(:,kdeg(k-1)) + r(:,kint(k))).*z(:,k) + r(:,krec(k)).*z(:,k+1);
    dydt(:,nT) = r(:,3*nT-3).*z(:,nT-1) - (r(:,3*nT-2)+r(:,3*nT-1)).*z(:,nT);
end
dydt = reshape(dydt,[ny*nT,1]);

val = sum(sum(abs(dydt) > tol));
isterm = 1;
dir = -1;

end