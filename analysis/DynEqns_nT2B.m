function dydt = DynEqns_nT2B(t,y)

% call the global variables. These are number of tiers, the vectors of rate 
% and feedback parameters, concentrations of ligand (a distribution) and 
% receptors (constant) at current position, the feedback topology under
% investigation, and the number of cells in y-direction (or ensemble size)
global nT
global rp
global fp
global Lx
global psi
global phi
global top
global ny

% ODE15s accepts only vectors as inputs so we need to convert (flatten) the 
% "current state matirx" into a vector before writing its dynamics. A similar
% thing is done for the vector. It's made into a corresponding matrix
% artificially for ease of use in the dynamics when considering feedbacks.
z = reshape(y,[ny,2*nT+1]);
r = repmat(rp,[ny,1]);

% the feedback topology vector encodes the rate ID as its second element. 
% However, because we reduce the number of rates by considering the desorption 
% rates as unity, the rate ID needs to be adjusted here accordingly. If it comes 
% after the first desorption rate (rD) subtract 1 and if it comes after the second 
% desorption rate as well, subtract 1 further.
rate_id = top(2);
if rate_id <= 2 
    rate_id = rate_id;
elseif rate_id <= 3*nT+2
    rate_id = rate_id - 1;
else
    rate_id = rate_id - 2;
end

% The rate under feedback is written as bare rate modified by a Hill
% function. Extract the bare rate from rate paraeter vector (rp) using the
% rate ID of the rate under feedback. Use the first element of the topology 
% vector as the affector species's ID. The type of feedback is given by the
% third element of the topology vector.
r0 = rp(rate_id);
if top(3) == 0
    r(:,rate_id) = r0./(1 + (fp(1)*z(:,top(1))).^fp(2));
else
    r(:,rate_id) = r0*(1 + (fp(1)*z(:,top(1)).^fp(3))./(fp(2)^(-fp(3)) + z(:,top(1)).^fp(3)) );
end

% writing the dynamics for concentrations of all the molecular species in
% cells at the same x-position but at different y-positions. The molecular
% species are labelled by column and the cell along y-direction is labelled
% by row in matrix 'z'. Similiarly, the rate ID is labelled by column in
% rate vector 'r'.
rD = 1;
kD = 1;
if nT == 1
    dydt(:,1) = r(:,1).*Lx'.*(psi-z(:,1)) - (rD + r(:,2)).*z(:,1) - r(:,6*nT-1).*z(:,nT).*z(:,2*nT) + r(:,6*nT).*z(:,2*nT+1);
    dydt(:,2) = r(:,3).*Lx'.*(phi-z(:,2)) - (kD + r(:,4)).*z(:,2) - r(:,6*nT-1).*z(:,nT).*z(:,2*nT) + r(:,6*nT).*z(:,2*nT+1);
    dydt(:,3) = r(:,6*nT-1).*z(:,nT).*z(:,2*nT) - r(:,6*nT).*z(:,2*nT+1);
elseif nT == 2
    dydt(:,1) = r(:,1).*Lx'.*(psi-z(:,1)) - (rD + r(:,2)+r(:,3)).*z(:,1) + r(:,4).*z(:,2);
    dydt(:,2) = r(:,3).*z(:,1) - (r(:,4)+r(:,5)).*z(:,2) - r(:,6*nT-1).*z(:,nT).*z(:,2*nT) + r(:,6*nT).*z(:,2*nT+1);
    dydt(:,3) = r(:,6).*Lx'.*(phi-z(:,3)) - (kD + r(:,7)+r(:,8)).*z(:,3) + r(:,9).*z(:,4);
    dydt(:,4) = r(:,8).*z(:,3) - (r(:,9)+r(:,10)).*z(:,4) - r(:,6*nT-1).*z(:,nT).*z(:,2*nT) + r(:,6*nT).*z(:,2*nT+1);
    dydt(:,5) = r(:,6*nT-1).*z(:,nT).*z(:,2*nT) - r(:,6*nT).*z(:,2*nT+1);
else
    k = 2:1:nT-1;
    kint = 3:3:3*nT-3;
    krec = 4:3:3*nT-2;
    kdeg = 5:3:3*nT-1;
    dydt(:,1) = r(:,1).*Lx'.*(psi-z(:,1)) - (rD + r(:,2)+r(:,3)).*z(:,1) + r(:,4).*z(:,2);
    dydt(:,k) = r(:,kint(k-1)).*z(:,k-1) - (r(:,krec(k-1)) + r(:,kdeg(k-1)) + r(:,kint(k))).*z(:,k) + r(:,krec(k)).*z(:,k+1);
    dydt(:,nT) = r(:,3*nT-3).*z(:,nT-1) - (r(:,3*nT-2)+r(:,3*nT-1)).*z(:,nT) - r(:,6*nT-1).*z(:,nT).*z(:,2*nT) + r(:,6*nT).*z(:,2*nT+1);
    kint = 3*nT+2:3:6*nT-4;
    krec = 3*nT+3:3:6*nT-3;
    kdeg = 3*nT+4:3:6*nT-2;
    dydt(:,nT+1) = r(:,3*nT).*Lx'.*(phi-z(:,nT+1)) - (kD + r(:,3*nT+1)+r(:,3*nT+2)).*z(:,nT+1) + r(:,3*nT+3).*z(:,nT+2);
    dydt(:,nT+k) = r(:,kint(k-1)).*z(:,nT+k-1) - (r(:,krec(k-1)) + r(:,kdeg(k-1)) + r(:,kint(k))).*z(:,nT+k) + r(:,krec(k)).*z(:,nT+k+1);
    dydt(:,nT+nT) = r(:,6*nT-4).*z(:,nT+nT-1) - (r(:,6*nT-3)+r(:,6*nT-2)).*z(:,nT+nT) - r(:,6*nT-1).*z(:,nT).*z(:,2*nT) + r(:,6*nT).*z(:,2*nT+1);
    dydt(:,2*nT+1) = r(:,6*nT-1).*z(:,nT).*z(:,2*nT) - r(:,6*nT).*z(:,2*nT+1);
end
% Again, for ODE15s the time derivative (update) matrix has to be converted
% to a vector. We do this in a manner that is consistent with the opposite
% conversion (vector to matrix) done previously in this function.
dydt = reshape(dydt,[ny*(2*nT+1),1]);

end