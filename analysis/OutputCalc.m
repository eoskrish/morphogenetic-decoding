function theta = OutputCalc(param_vector,Lvals,xpts,channel)

% if you wish to check the solutions at every iteration of ix (position index),
% set checks_flag to 1
checks_flag = 0;

% set number of tiers and number of branches and global parameters for this
% kernel (parallel worker). To ensure that all the workers get the same
% assignment of these variables, load them from a previously saved txt file
global nT
global nB
% cp = load('ChannelArch.txt');
nT = channel(1); %cp(1);
nB = channel(2); %cp(2);
% determine the length of rate and weight vectors
len_rate_vec = 3*nT-1;
num_wt_params = nT - 1;

% extract the rate, receptor profile and feedback parameters and set them
% as global for each individual kernel
global fp
global rp
rp = param_vector(1:len_rate_vec);
rtp = param_vector(len_rate_vec+1:len_rate_vec+4);
fp = param_vector(len_rate_vec+5:end-num_wt_params);
% fp = [0 1 1];
if nT > 1
    wp = param_vector(end-num_wt_params+1:end);
else 
    wp = 1;
end
% load the previously saved ligand concentration (input) distribution
% Lvals = load('Lvals.txt');

% numerical parameters (constants)
global nx
global ny
global dx
global dxp
[nx,~,dx,dxp,~] = DeclareNumParams();
ny = size(Lvals,2);
x = linspace(0,1,nx);
% ny = 1;
nxpts = length(xpts);

% rc = load('rc.txt');
rc = channel(3);
if rc == 0
    psix = rtp(2) + ( rtp(3)*(x.^rtp(1)) )./( (rtp(4)^rtp(1)) + (x.^rtp(1)) ); 
%     rf = 'A';
elseif rc == 1
    psix = rtp(2) + ( rtp(3) )./( 1 + ( (x/rtp(4)).^rtp(1)) ) ; 
%     rf = 'B';
end
% psix(:) = 1;

% uncomment below if you wish to check psi profiles. 
% DO NOT uncomment when running an Optimisation! 
% plot(psix)
% pause()
% close all

global psi
global top
global Lx
% top = load(['top_' num2str(nT) 'T' num2str(nB) 'B.txt']);
top = channel(4:6);

% output directory to store errors/warnings 
% outdir = ['Opt_' num2str(nT) 'T' num2str(nB) 'B_' num2str(top(1)) '-' num2str(top(2)) '-' num2str(top(3))];

maxt = 2^25;
tspan = [0 maxt];
y0 = zeros(ny,nT);
opts = odeset('Events',@eventfun_nT1B);
SSsoln = zeros(nxpts,ny,nT);
count1 = 0;
count2 = 0;
for ix = 1 : nxpts
    psi = psix(xpts(ix));
    Lx = Lvals(ix,1:ny);
    [t,y] = ode15s(@(t,y) DynEqns_nT1B(t,y),tspan,y0,opts);
    SSsoln(ix,:,:) = reshape(y(end,:),[1,ny,nT]);
    if t(end) == maxt
%         writematrix([param_vector ix],[outdir '/' 'SteadyStateErrors' rf '.txt'],'WriteMode','append')
        count1 = count1 + 1;
    else 
        checkDyDt = DynEqns_nT1B(t(end),y(end,:));
        if sum(sum(abs(checkDyDt) > 1e-4)) ~= 0
%             writematrix([param_vector ix],[outdir '/' 'SteadyStateErrors' rf '.txt'],'WriteMode','append')
            count2 = count2 + 1;
        end
    end

    % checks
    if checks_flag == 1
        for iT = 1 : nT
            y = reshape(y,[size(y,1),ny,nT]);
            figure(iT)
            plot(t,y(:,:,iT))
            xlabel('time, 1/rD units')
            ylabel('concenctrations')
            title([num2str(iT) '-th tier, x = ' num2str(dx*(ix-1))])
        end
        pause()
    end
end
% the output theta is a tier-dependent weighted sum of the concentration of signalling
% species in all the tiers
% defininng the weight matrix
if nT > 1
    w = zeros(nxpts,ny,nT);
    w(1,1,1:nT-1) = wp;
    w(1,1,nT) = 1-sum(wp);
    for i = 1 : nT
        w(:,:,i) = w(1,1,i);
    end
    theta = sum(w.*SSsoln(:,:,1:nT),3);
else 
    theta = SSsoln(:,:,1);
end

end